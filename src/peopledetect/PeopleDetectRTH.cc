#include "../Resource.hh"
#include "PeopleDetectRTH.hh"
#include "../Debug.hh"
#include <math.h>

#include "../tools/scoped-timer.hh"
#include "../settings-manager.hh"

namespace peopledetect {

  PeopleDetectRTH::PeopleDetectRTH(unsigned int targetFps):
    initialised(false),
    threshold_(0.0f),
    targetFps_(targetFps) {

  }

  PeopleDetectRTH::~PeopleDetectRTH() {

  }


  void PeopleDetectRTH::update(cv::Mat mat) {
    //cv::Mat copy;
    //mat.copyTo(copy);
    tools::ScopedTimer<std::chrono::milliseconds> timer(std::chrono::milliseconds(1000 / targetFps_));
    updateCopy(mat); // already copy in window.cc
    lastExecutionTime_ = timer.getElapsedTime();
  }

  void PeopleDetectRTH::updateCopy(cv::Mat copy) {
    std::vector<cv::Rect> found;
    const int width = copy.cols;
    const int height = copy.rows;
    people_compute(copy, found);

    {
      const std::lock_guard<std::mutex> lock(mutex_); // lock scope
      humanFound.clear();
      for(unsigned int i = 0; i < found.size(); i++) {
	const cv::Rect currentRect = found[i];
        float currentLikelihood = 0.0f;
	if(isValidRect(currentRect)) {
	  cv::Rect center = getCenter(currentRect);
	  resizeRect(center, width, height);
	  const cv::Mat centerMat = copy(center);
	  currentLikelihood = extractLikelihood(centerMat);
	}
	//std::cout << currentLikelihood << " ";
	const HumanRTH currentHuman(currentRect, currentLikelihood);
	humanFound.push_back(currentHuman); // add human
      }
      //std::cout << std::endl;
    }
  }

  void PeopleDetectRTH::resizeRect(cv::Rect& rect, const int width, const int height) {
    if(rect.x < 0) {
      rect.width = rect.width + rect.x;
      rect.x = 0;
    }
    if(rect.y < 0) {
      rect.height = rect.height + rect.y;
      rect.y = 0;
    }
    if(rect.x + rect.width > width) {
      rect.width = width - rect.x;
    }
    if(rect.y + rect.height > height) {
      rect.height = height - rect.y;
    }
  }

  std::chrono::milliseconds PeopleDetectRTH::getLastExecutionTime() const {
    return lastExecutionTime_;
  }

  void PeopleDetectRTH::removeTarget() {
    initialised = false;
  }

  bool PeopleDetectRTH::isValidRect(const cv::Rect& rect) {
    return rect.width > 10 && rect.height > 10;
  }

  HumanInfo* PeopleDetectRTH::setNewTarget(const int x, const int y) {
    // must call recalibrate
    const std::lock_guard<std::mutex> lock(mutex_); // lock scope
    double bestDistance = std::numeric_limits<double>::max();
    int bestIndex = -1;
    const unsigned int size = humanFound.size();
    for(unsigned int i = 0; i < size; i++) {
      const cv::Rect currentRect = humanFound[i].getRectangle();
      
      if(isInside(currentRect, x, y)) {
	const double currentDistance = getDistance(currentRect, x, y);
	if(currentDistance < bestDistance) {
	  bestDistance = currentDistance;
	  bestIndex = i;
	}
      }
    }

    if(bestIndex == -1) {
      return nullptr;
    } else {
      return new HumanRTH(humanFound[bestIndex]);
    }
  }

  float PeopleDetectRTH::getThreshold() {
    return threshold_;
  }

  bool PeopleDetectRTH::isInside(const cv::Rect& rect, const int x, const int y) {
    return x >= rect.x && y >= rect.y
      && x < rect.x + rect.width && y < rect.y + rect.height;
  }

  double PeopleDetectRTH::getDistance(const cv::Rect& rect, const double x2, const double y2) {
    return getDistance(rect.x + (rect.width >> 1), rect.y + (rect.height >> 1), x2, y2);
  }

  double PeopleDetectRTH::getDistance(const double x1, const double y1,
				      const double x2, const double y2) {
    double xx = x1 - x2;
    double yy = y1 - y2;
    xx = xx * xx;
    yy = yy * yy;
    return sqrt(xx + yy);
  }

  cv::Rect PeopleDetectRTH::getCenter(const cv::Rect rect) {
    int x = rect.x;
    int y = rect.y;
    int width = rect.width;
    int height = rect.height;

    x = x + (width >> 2);
    y = y + (height >> 2);
    width = width >> 1;
    height = height >> 1;

    if(Debug::debug) {
      assert(width > 0);
      assert(height > 0);
    }

    return cv::Rect(x, y, width, height);
  }

  float PeopleDetectRTH::extractLikelihood(const cv::Mat src) {
    if(!initialised) {
      return 0.0f;
    }
    cv::Mat hsv;
    cv::resize(src, hsv, cv::Size(WIDTH, HEIGHT));
    //cv::cvtColor(hsv, hsv, CV_RGB2HSV);
    if(Debug::debug) {
      assert(hsv.cols == WIDTH);
      assert(hsv.rows == HEIGHT);
    }

    // .at<cv::Vec3b>(x,y).val[0]
    int index = 0;
    double totalDistance = 0.0d;
    for(int i = 0; i < WIDTH; i++) {
      for(int j = 0; j < HEIGHT; j++) {
	if(Debug::debug) {
	  assert(index < SIZE);
	  assert(stockVariance[index] >= 1.0d);
	}
	//const double toAdd = circleDistance(stockH[index], hsv.at<cv::Vec3b>(i, j).val[0]);
	const double toAdd = normalDistance(stockR[index],
					    hsv.at<cv::Vec3b>(i, j).val[0])
	  + normalDistance(stockG[index],
			   hsv.at<cv::Vec3b>(i, j).val[1])
	  + normalDistance(stockG[index],
			   hsv.at<cv::Vec3b>(i, j).val[2]);
	totalDistance = totalDistance + (toAdd / stockVariance[index]);
	index++;
      }
    }

    const double ret = totalDistance / (double)MAX_VALUE;
    if(Debug::debug) {
      assert(ret >= 0.0d);
      assert(ret <= 1.0d);
    }
    return 1.0f - ret;
  }

  double PeopleDetectRTH::min(double nbr1, double nbr2) const {
    if(nbr1 < nbr2) {
      return nbr1;
    } else {
      return nbr2;
    }
  }

  double PeopleDetectRTH::normalDistance(double nbr1, double nbr2) const {
    double ret = nbr1 -nbr2;
    if(ret < 0.0d) {
      ret = -ret;
    }
    if(Debug::debug) {
      assert(ret >= 0.0d);
    }
    return ret;
  }

  double PeopleDetectRTH::circleDistance(double nbr1, double nbr2) const {
    if(Debug::debug) {
      assert(nbr1 >= 0.0d);
      assert(nbr2 >= 0.0d);
      assert(nbr1 <= 255.0d);
      assert(nbr2 <= 255.0d);
    }

    double minValue;
    double maxValue;
    if(nbr1 > nbr2) {
      minValue = nbr2;
      maxValue = nbr1;
    } else {
      minValue = nbr1;
      maxValue = nbr2;
    }

    if(Debug::debug) {
      assert(maxValue >= minValue);
      assert(maxValue - minValue >= 0.0d);
      assert(minValue + 255.0d - maxValue >= 0.0d);
    }

    const double ret = min(maxValue - minValue,
			   minValue + 255.0d - maxValue);

    return ret;
  }

  std::vector<HumanInfo*>* PeopleDetectRTH::getCopyHumans() {
    const std::lock_guard<std::mutex> lock(mutex_); // lock scope
    std::vector<HumanInfo*>* ret = new std::vector<HumanInfo*>();
    for(unsigned int i = 0; i < humanFound.size(); i++) {
      ret->push_back(new HumanRTH(humanFound[i]));
    }
    return ret;
  }

  void PeopleDetectRTH::recalibrateTarget(const HumanInfo& target) {
    const std::lock_guard<std::mutex> lock(mutex_); // lock scope
    const cv::Mat src = Resource::getInstance().getCopyMat(false);
    cv::Mat hsv;
    {
      const cv::Rect rect = target.getRectangle(); // get rectangle
      cv::Rect center = getCenter(rect); // center to the person
      resizeRect(center, src.cols, src.rows);
      cv::Mat good_size;
      cv::resize(src(center), good_size, cv::Size(WIDTH, HEIGHT)); // convert to 64*128
      if(Debug::debug) {
	assert(good_size.cols == WIDTH);
	assert(good_size.rows == HEIGHT);
      }
      //cv::cvtColor(good_size, hsv, CV_RGB2HSV); // convert to HSV
      good_size.copyTo(hsv); // don't convert to hsv
      if(Debug::debug) {
	assert(hsv.cols == WIDTH);
	assert(hsv.rows == HEIGHT);
      }
    } // here hsv ready (good shape and good color)

    if(initialised) {
      updateTarget(hsv);
    } else {
      initialiseTarget(hsv);
      initialised = true;
    }

    const float newLikelihood = extractLikelihood(hsv) * 0.9f;
    //std::cout << newLikelihood << std::endl;
    threshold_ = newLikelihood;
  }

  void PeopleDetectRTH::initialiseTarget(cv::Mat& hsv) {
      if(Debug::debug) {
	assert(hsv.cols == WIDTH);
	assert(hsv.rows == HEIGHT);
      }
      
      int index = 0;
      for(int i = 0; i < WIDTH; i++) {
	for(int j = 0; j < HEIGHT; j++) {
	  if(Debug::debug) {
	    assert(index < SIZE);
	  }
	  //stockH[index] = hsv.at<cv::Vec3b>(i, j).val[0];
	  stockR[index] = hsv.at<cv::Vec3b>(i, j).val[0];
	  stockG[index] = hsv.at<cv::Vec3b>(i, j).val[1];
	  stockB[index] = hsv.at<cv::Vec3b>(i, j).val[2];
	  stockVariance[index] = 1.0d;
	  index++;
	}
      }
      
  }

  void PeopleDetectRTH::updateTarget(cv::Mat& hsv) {
      if(Debug::debug) {
	assert(hsv.cols == WIDTH);
	assert(hsv.rows == HEIGHT);
	assert(DEPRECIATION_COEF <= 1.0d);
	assert(INVERSE_COEF <= 1.0d);
      }

      cv::imshow("bidule", hsv);
      //cv::waitKey(10);

      int index = 0;
      for(int i = 0; i < WIDTH; i++) {
	for(int j = 0; j < HEIGHT; j++) {
	  if(Debug::debug) {
	    assert(index < SIZE);
	    assert(stockVariance[index] >= 1.0d);
	  }
	  const double toAdd = normalDistance(stockR[index],
					      hsv.at<cv::Vec3b>(i, j).val[0])
	    + normalDistance(stockG[index],
			     hsv.at<cv::Vec3b>(i, j).val[1])
	    + normalDistance(stockB[index],
			     hsv.at<cv::Vec3b>(i, j).val[2]);
	  //const double newVariance = (stockVariance[index] * INVERSE_COEF) +
	  //(circleDistance(hsv.at<cv::Vec3b>(i, j).val[0], stockH[index]) * DEPRECIATION_COEF);
	  const double newVariance = (stockVariance[index] * INVERSE_COEF) +
	    (toAdd * DEPRECIATION_COEF);
	  if(newVariance >= 1.0d) {
	    stockVariance[index] = newVariance;
	  } else {
	    stockVariance[index] = 1.0d;
	  }
	  //const double newH = (hsv.at<cv::Vec3b>(i, j).val[0] * DEPRECIATION_COEF)
	  //+ (stockH[index] * INVERSE_COEF);
	    //stockH[index] = newH;
	  const double newR = (hsv.at<cv::Vec3b>(i, j).val[0] * DEPRECIATION_COEF)
	    + (stockR[index] * INVERSE_COEF);
	  const double newG = (hsv.at<cv::Vec3b>(i, j).val[1] * DEPRECIATION_COEF)
	    + (stockG[index] * INVERSE_COEF);
	  const double newB = (hsv.at<cv::Vec3b>(i, j).val[2] * DEPRECIATION_COEF)
	    + (stockB[index] * INVERSE_COEF);
	  stockR[index] = newR;
	  stockG[index] = newG;
	  stockB[index] = newB;

	  index++;
	}
      }
      
  }

  void PeopleDetectRTH::people_compute(cv::Mat& src,
				       std::vector<cv::Rect>& found) {
    if(SettingsManager::getInstance().isGPUEnabled()) {
      gpu_compute(src, found);
    } else {
      cpu_compute(src, found);
    }
  }

  // CPU
  void PeopleDetectRTH::cpu_compute(cv::Mat& src, std::vector<cv::Rect>& found)
  {
    cv::HOGDescriptor hog;
    hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
    /*
    hog.detectMultiScale(src,
			 found, 
			 0, //hit threshold
			 cv::Size(8, 8),
			 //Stride/le pas de décalage 
			 cv::Size(32,32),
			 //padding: added pixels on the borders
			 1.05, //scale
			 2 //group threshold
			 );
    */
    hog.detectMultiScale(src,
			 found, 
			 0, //hit threshold
			 cv::Size(8, 8),
			 //Stride/le pas de décalage 
			 cv::Size(0, 0),
			 //padding: added pixels on the borders
			 1.05, //scale
			 2 //group threshold
			 );
  }

  //GPU
  void PeopleDetectRTH::gpu_compute(cv::Mat& src, std::vector<cv::Rect>& found)
  {
    cv::Size win_size(64,128); //(64, 128) or (48, 96)
    cv::Size win_stride(8,8);
    
    std::vector<float> detector = cv::gpu::HOGDescriptor::getPeopleDetector64x128();
    
    cv::gpu::HOGDescriptor gpu_hog(win_size, 
				   cv::Size(16, 16), 
				   cv::Size(8, 8), 
				   cv::Size(8, 8), 
				   9,
				   cv::gpu::HOGDescriptor::DEFAULT_WIN_SIGMA, 
				   0.2,
				   true, //gamma correction
				   cv::gpu::HOGDescriptor::DEFAULT_NLEVELS);
    
    gpu_hog.setSVMDetector(detector);
    
    cv::Mat img_aux, img, img_to_show;
    cv::gpu::GpuMat gpu_img;
    
    // Change format of the image
    cv::cvtColor(src, img_aux, CV_BGR2BGRA);

    // Resize image
    //resize(img_aux, img, Size(args.width, args.height));
    img = img_aux;
    img_to_show = img;
    
    gpu_hog.nlevels = 13; //Max number of HOG window scales
    
    // Perform HOG classification
    gpu_img.upload(img);
    gpu_hog.detectMultiScale(gpu_img,
			     found, 
			     0,//hit_threshold, 
			     cv::Size(8,8),//win_stride,
			     cv::Size(0, 0), //padding Should be 0
			     // to keep cpu compatibility
			     1.05,//scale,
			     2);//group_threshold

  }
  
  
}
