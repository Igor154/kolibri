#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/gpu/gpu.hpp"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <mutex>
#include <limits>

#include "HumanInfo.hh"

namespace peopledetect {

  class PeopleDetectHelper {
  public:

    virtual ~PeopleDetectHelper();

    virtual void update(cv::Mat mat);
    virtual std::vector<HumanInfo*>* getCopyHumans();
    virtual void recalibrateTarget(const HumanInfo& h);
    virtual std::chrono::milliseconds getLastExecutionTime() const;
    virtual void removeTarget();
    virtual HumanInfo* setNewTarget(int x, int y);
    virtual float getThreshold();

  };

}
