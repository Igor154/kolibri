#include "video-label.hh"

#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

namespace ui
{
  static QImage mat2QImage(const cv::Mat& src)
  {
    cv::Mat temp;
    cvtColor(src, temp, CV_BGR2RGB);
    QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
    dest.bits();
    return dest;
  }

  VideoLabel::VideoLabel(const QString& text, QWidget* parent)
    : QLabel(parent)
    , _empty(true)
  {
    setText(text);
  }

  void VideoLabel::update(const cv::Mat& frame)
  {
    QImage image = mat2QImage(frame);
    setPixmap(QPixmap::fromImage(image));
    setFixedSize(image.size());
    _empty = false;
  }

  void VideoLabel::mousePressEvent(QMouseEvent* event)
  {
    if (!_empty)
      emit clicked(event->x(), event->y());
  }
}
