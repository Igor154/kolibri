#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>
#include <vector>
#include "human.hh"

namespace peopledetect
{
  class Helper
  {
  public:
    static const bool consolePrint = false;
    static const bool drawWindow = false;
    //nb of updates accepted before setting currtarget to null
    static const unsigned int nbUpWithoutTargetTolerance = 1000000;
    static const bool doubleSizedImage = false;
    //PEOPLEDETECT PARAMS CPU
    static const unsigned int stride = 8; //taille de décalage
    //nb of updates before list is cleared
    static const unsigned int latestHumanList_timeout = 6;

    // removes negative values from x, y and sets them to 0
    // checks that x + width dont get out of bounds
    static void rect_remove_neg_values(cv::Rect& rect, int srcWidth, int srcHeight)
    {
      if (rect.x < 0)
	rect.x = 0;
      if (rect.y < 0)
	rect.y = 0;
      if(rect.x + rect.width >= srcWidth)
	rect.width = srcWidth - rect.x;
      if(rect.y + rect.height >= srcHeight)
	rect.height = srcHeight - rect.y;
    
    }
    static void crop(std::vector<cv::Rect>& rect, int srcWidth, int srcHeight)
    {
      for (unsigned int i = 0; i < rect.size(); ++i)
      {
      if (rect[i].x < 0)
	rect[i].x = 0;
      if (rect[i].y < 0)
	rect[i].y = 0;
      if(rect[i].x + rect[i].width >= srcWidth)
	rect[i].width = srcWidth - rect[i].x;
      if(rect[i].y + rect[i].height >= srcHeight)
	rect[i].height = srcHeight - rect[i].y;
      }
    }
    static void sanity_check(std::vector<cv::Rect>& rect)
    {
      int end = (int)(rect.size());
      for (int i = 0; i < end; ++i)
	{
	  //proportions nimp
	  float max = (float)std::max(rect[i].width, rect[i].height);
	  float min = (float)std::min(rect[i].width, rect[i].height);
	  if (max / min > 3.)
	    {
	      rect.erase(rect.begin() + i);
	      i--;
	      end--;
	    }
	}
    }
  
    
  };

}//namespace

