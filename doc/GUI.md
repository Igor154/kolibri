# Introduction

The Graphical User Interface allows you to:

* connect to the drone
* control the drone manually with a keyboard or a joystick
* monitor real-time flight data and execution
* start the tracking algorithms
* record the video

# Usage

## Start the program

The main binary is located in the build folder:

```
./build/src/kolibri
```

## Connect to the drone

1. Power on the drone
2. Connect to the drone Wi-Fi
3. Click on the *Connect* button

On successful connection, the live camera feed is displayed within the program.

If you are unable to connect to the drone, please check the following items:

* The drone is powered on and has sufficient battery life remaining
* The PC is properly connected to the drone and is assigned an IP address (usually `192.168.1.2`)

If you are still unable to connect, please check that the drone is using the latest available firmware:

- AR.Drone 2.0, use the [official mobile application](https://play.google.com/store/apps/details?id=com.parrot.freeflight&hl=fr) to check and update the firmware

- AR.Drone 1.0, older firmwares use *ad hoc* Wi-Fi connection that is frequently ignored on modern system (for example Android, Windows 8 and greater) and is likely to be the cause of the issue. To update the drone in this case:

    - tweak your system to enable *ad hoc* Wi-Fi (for example on [Windows](https://social.technet.microsoft.com/Forums/windows/en-US/56ff83ff-1f15-4fc1-aa37-6651340d46fa/windows-81-connecting-to-ad-hoc-networks?forum=w8itpronetworking))
    - connect to the drone
    - follow [this method](http://ardrone2.parrot.com/update-ardrone-1/) to update the firmware

## Control the drone

Before starting the auto-pilot mode, the drone can be manually controlled.

### Menu

The program toolbar includes *Take off* and *Land* buttons.

### Keyboard

* Take off: `P`
* Land: `SPACE`
* Move forward: `Z`
* Move backward: `S`
* Move left: `Q`
* Move right: `D`
* Turn left: `A`
* Turn right: `E`
* Increase altitude: `H`
* Decrease altitude: `L`

### Joystick

(using *PlayStation* labels)

* Take off: `X`
* Land: `O`
* Move: left joystick
* Turn: right joystick
* Increase altitude: `R2`
* Decrease altitude: `L2`

## Monitoring

The bottom part of the program is used to monitor the execution.

### Altitude

To view and change the drone current altitude.

### Navigation data

It displays real time data about the drone:

* Battery: battery level
* Altitude: altitude in mm
* Theta: front/back angle in mdeg
* Phi: left/right angle in mdeg
* Psi: absolute left/right angle in mdeg
* Vx: x velocity
* Vy: y velocity
* Vz: z velocity

### Monitoring

It displays information about the detection algorithms.

* Automatic flight: *inactive* or *active*
* People detection time: *not running* or the execution time in ms on 1 frame
* Stabilisation time: *not running* or the execution time in ms on 1 frame

### Start the tracking

1. Click on the *Start* button
2. Click on the target directly on the live camera feed (the green rectangle should turn yellow)
4. Press `O` on the keyboard to start the auto-pilot.

### Record the video

To record the video from the drone camera, click on the *Start recording* button. To stop it, click on the *Stop recording* button.

A raw H.264 file will be generated in the current directory with the name: `recording-DATE.h264`. To convert it to the standard MP4 format, simply use the following command:

```
ffmpeg -f h264 -i recording-DATE.h264 -vcodec copy myrecording.mp4
```

### Settings

* Enable the GPU: check *Settings* > *Enable GPU*
