#ifndef TOOLS_SCOPED_TIMER_HH
# define TOOLS_SCOPED_TIMER_HH

# include "timer.hh"

namespace tools
{
  // Used to make sure that a scope lasts for at least a given minimal duration.
  template <typename T>
  class ScopedTimer
  {
  public:
    ScopedTimer(T minimalDuration);
    ~ScopedTimer();

    T getElapsedTime() const;

  private:
    Timer<T> _timer;
    T _minimalDuration;
  };
}

# include "scoped-timer.hxx"

#endif /* !TOOLS_SCOPED_TIMER_HH */
