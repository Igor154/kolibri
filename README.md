# Introduction

The **Kolibri** project uses *Parrot AR.Drone* to track and record people autonomously.

**Key features:**

* Connect and control *Parrot AR.Drone* 1.0 and 2.0
* Monitor in-flight data
* Select a target to follow using auto-pilot
* Record the drone camera video

**Key algorithms:**

* People detection using trained SVM on HOG descriptors
* Movement tracking using optical flow
* Movement prediction using Kalman-like algorithm

# Requirements

* a drone *Parrot AR.Drone* 2.0 (although it works with the 1.0, the results will be disappointing)
* a powerful multi-core processor
* (optional) a discrete GPU

# Testing the project

See `doc/INSTALL.md` for install instructions.

See `doc/GUI.md` for usage instructions.
