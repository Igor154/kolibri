#ifndef UI_VIDEO_LABEL_HH
# define UI_VIDEO_LABEL_HH

# include <opencv2/core/core.hpp>
# include <QtWidgets>

namespace ui
{
  class VideoLabel: public QLabel
  {
    Q_OBJECT

  public:
    explicit VideoLabel(const QString& text = "", QWidget* parent = 0);

    void update(const cv::Mat& frame);

  signals:
    void clicked(int x, int y);

  protected:
    void mousePressEvent(QMouseEvent* event);

  private:
    bool _empty;
  };
}

#endif
