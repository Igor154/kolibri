#pragma once

#include <opencv2/core/core.hpp>

#ifdef TEST_WITH_VIDEO
# include <mutex>
#endif

class Resource {

public:
  static Resource& getInstance();
  cv::Mat getCopyMat(bool forCollision) const;

  size_t getWidth() const;
  size_t getHeight() const;

#ifdef TEST_WITH_VIDEO
  void setMat(const cv::Mat& mat);

private:
  mutable std::mutex mutex;
  cv::Mat currentMat;
#endif
};
