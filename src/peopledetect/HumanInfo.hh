#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/gpu/gpu.hpp"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <mutex>
#include <limits>

namespace peopledetect {

  class HumanInfo {
  public:

    virtual ~HumanInfo();

    virtual const cv::Rect& getRectangle() const;
    virtual float getLikelihood() const;

  };

}
