#include "navdata.hh"

#ifdef __cplusplus
extern "C"
{
#endif

#include <ardrone_tool/Navdata/ardrone_navdata_client.h>
#include <stdio.h>

/* Initialization local variables before event loop  */
inline C_RESULT navdata_client_init(void*)
{
  return C_OK;
}

/* Receving navdata during the event loop */
inline C_RESULT navdata_client_process(const navdata_unpacked_t* const navdata)
{
  const navdata_demo_t* nd = &navdata->navdata_demo;

  ardrone::NavData::getInstance().setBatteryLevel(nd->vbat_flying_percentage);
  ardrone::NavData::getInstance().setState(navdata->ardrone_state);
  ardrone::NavData::getInstance().setAltitude(nd->altitude);
  ardrone::NavData::getInstance().setTheta(nd->theta);
  ardrone::NavData::getInstance().setPhi(nd->phi);
  ardrone::NavData::getInstance().setPsi(nd->psi);
  ardrone::NavData::getInstance().setVx(nd->vx);
  ardrone::NavData::getInstance().setVy(nd->vy);
  ardrone::NavData::getInstance().setVz(nd->vz);

  return C_OK;
}

/* Relinquish the local resources after the event loop exit */
inline C_RESULT navdata_client_release()
{
  return C_OK;
}

/* Registering to navdata client */
BEGIN_NAVDATA_HANDLER_TABLE
  NAVDATA_HANDLER_TABLE_ENTRY(navdata_client_init, navdata_client_process, navdata_client_release, NULL)
END_NAVDATA_HANDLER_TABLE

#ifdef __cplusplus
}
#endif

namespace ardrone
{
  NavData::NavData()
    : _battery_level(0)
    , _state(0)
    , _altitude(0)
    , _theta(0)
    , _phi(0)
    , _psi(0)
    , _vx(0)
    , _vy(0)
    , _vz(0)
    , _roll(0)
    , _pitch(0)
    , _yaw(0)
    , _height(0)
  {
  }

  NavData& NavData::getInstance()
  {
    static NavData instance;
    return instance;
  }

  void NavData::setBatteryLevel(unsigned battery_level)
  {
    _battery_level = battery_level;
  }

  void NavData::setState(unsigned state)
  {
    _state = state;
  }

  void NavData::setAltitude(int altitude)
  {
    _altitude = altitude;
  }

  void NavData::setTheta(double theta)
  {
    _theta = theta;
  }

  void NavData::setPhi(double phi)
  {
    _phi = phi;
  }

  void NavData::setPsi(double psi)
  {
    _psi = psi;
  }

  void NavData::setVx(double vx)
  {
    _vx = vx;
  }

  void NavData::setVy(double vy)
  {
    _vy = vy;
  }

  void NavData::setVz(double vz)
  {
    _vz = vz;
  }

  void NavData::setRoll(double roll)
  {
    _roll = roll;
  }

  void NavData::setPitch(double pitch)
  {
    _pitch = pitch;
  }

  void NavData::setYaw(double yaw)
  {
    _yaw = yaw;
  }

  void NavData::setHeight(double height)
  {
    _height = height;
  }

  unsigned NavData::getBatteryLevel() const
  {
    return _battery_level;
  }

  unsigned NavData::getState() const
  {
    return _state;
  }

  int NavData::getAltitude() const
  {
    return _altitude;
  }

  double NavData::getTheta() const
  {
    return _theta;
  }

  double NavData::getPhi() const
  {
    return _phi;
  }

  double NavData::getPsi() const
  {
    return _psi;
  }

  double NavData::getVx() const
  {
    return _vx;
  }

  double NavData::getVy() const
  {
    return _vy;
  }

  double NavData::getVz() const
  {
    return _vz;
  }

  double NavData::getRoll() const
  {
    return _roll;
  }

  double NavData::getPitch() const
  {
    return _pitch;
  }

  double NavData::getYaw() const
  {
    return _yaw;
  }

  double NavData::getHeight() const
  {
    return _height;
  }
}
