#include "drone.hh"

# include <chrono>
# include <iostream>

#ifdef __cplusplus
extern "C"
{
#endif

#include <ardrone_api.h>
#include <ardrone_tool/UI/ardrone_input.h>
#include <ardrone_tool/ardrone_version.h>

#ifdef __cplusplus
}
#endif

#include "navdata.hh"

namespace ardrone
{
  static float normalize(float value, float max) {
    if(value < -max) {
      return -max;
    }
    if(value > max) {
      return max;
    }
    return value;
  }

  Drone::Drone()
    : controller_(&Drone::run, this)
    , pitch_(0.0f)
    , roll_(0.0f)
    , yaw_(0.0f)
    , height_(0.0f)
    , targetHeight_(defaultAltitude)
    , emergencyState_(false)
    , exit_(false) {
  }

  Drone::~Drone() {
    exit_ = true;
    controller_.join();
  }

  Drone& Drone::getInstance() {
    static Drone instance;
    return instance;
  }

  int Drone::getVersion() const {
    return ARDRONE_VERSION();
  }

  bool Drone::isVersion2() const {
    return getVersion() == 2;
  }

  bool Drone::isVersion1() const {
    return getVersion() == 1;
  }

  void Drone::init() {
  }

  void Drone::takeOff() {
    ardrone_at_set_flat_trim();
    ardrone_tool_input_reset();
    ardrone_tool_set_ui_pad_start(1);
    stopMoving();
  }

  void Drone::land() {
    ardrone_tool_set_ui_pad_start(0);
  }

  void Drone::moveForward(float speed) {
    pitch_ = normalize(-speed, 1.0f);
  }

  void Drone::moveBackward(float speed) {
    pitch_ = normalize(speed, 1.0f);
  }

  void Drone::stopMovingHorizontally() {
    pitch_ = 0.0f;
  }

  void Drone::moveLeft(float speed) {
    roll_ = normalize(-speed, 1.0f);
  }

  void Drone::moveRight(float speed) {
    roll_ = normalize(speed, 1.0f);
  }

  void Drone::stopMovingLaterally() {
    roll_ = 0.0f;
  }

  void Drone::turnLeft(float speed) {
    yaw_ = normalize(-speed, 1.0f);
  }

  void Drone::turnRight(float speed) {
    yaw_ = normalize(speed, 1.0f);
  }

  void Drone::stopTurning() {
    yaw_ = 0.0f;
  }

  void Drone::stopMoving() {
    stopMovingHorizontally();
    stopMovingLaterally();
    stopTurning();
  }

  void Drone::setRelativeAltitude(int height) {
    int newHeight = targetHeight_ + height;
    if (newHeight >= minAltitude && newHeight <= maxAltitude)
      targetHeight_ = newHeight;
  }

  void Drone::setAbsoluteAltitude(int height) {
    if (height >= minAltitude && height <= maxAltitude)
      targetHeight_ = height;
  }

  void Drone::increaseAltitude() {
    setRelativeAltitude(altitudeStep);
  }

  void Drone::decreaseAltitude() {
    setRelativeAltitude(-altitudeStep);
  }

  void Drone::run() {
    std::chrono::duration<int, std::milli> waitingTime(commandsSendingDelayInMs);

    while(!exit_) {
      std::this_thread::sleep_for(waitingTime);

      if (ardrone::NavData::getInstance().getState() & ARDRONE_EMERGENCY_MASK) // Emergency state
      {
        if (!emergencyState_)
        {
          ardrone_tool_set_ui_pad_start(0);
          emergencyState_ = true;
        }

        ardrone_tool_set_ui_pad_select(1);
      }
      else
      {
        if (emergencyState_)
        {
          ardrone_tool_set_ui_pad_select(0);
          emergencyState_ = false;
        }

	const int currentAltitude = ardrone::NavData::getInstance().getAltitude();
        const float heightDiff = targetHeight_ - currentAltitude;

        if (abs(heightDiff) < altitudeErrorMargin)
        {
          height_ = 0.0f;
        }
        else
        {
          height_ = normalize(heightDiff / altitudeNormalization, 1.0f);
        }

        ardrone_tool_set_progressive_cmd(1 << ARDRONE_PROGRESSIVE_CMD_ENABLE,
                                         roll_,
                                         pitch_,
                                         height_,
                                         yaw_,
                                         0.0f,
                                         0.0f);

        ardrone::NavData::getInstance().setRoll(roll_);
        ardrone::NavData::getInstance().setPitch(pitch_);
        ardrone::NavData::getInstance().setYaw(yaw_);
        ardrone::NavData::getInstance().setHeight(height_);
      }
    }
  }
}
