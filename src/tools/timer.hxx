#ifndef TOOLS_TIMER_HXX
# define TOOLS_TIMER_HXX

namespace tools
{
  template <typename T>
  Timer<T>::Timer()
    : _start(std::chrono::high_resolution_clock::now())
  {
  }

  template <typename T>
  void Timer<T>::reset()
  {
    _start = std::chrono::high_resolution_clock::now();
  }

  template <typename T>
  T Timer<T>::getElapsedTime() const
  {
    auto now = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<T>(now - _start);
  }

  template <typename T>
  std::ostream& operator<<(std::ostream& os, const Timer<T>& t)
  {
    os << t.getElapsedTime().count();
    return os;
  }
}

#endif /* !TOOLS_TIMER_HXX */
