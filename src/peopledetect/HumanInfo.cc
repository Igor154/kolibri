#include "HumanInfo.hh"

namespace peopledetect {

  HumanInfo::~HumanInfo() {

  }

  const cv::Rect& HumanInfo::getRectangle() const {
    assert(false);
    return *(new cv::Rect(0, 0, 0, 0));
  }
  
  float HumanInfo::getLikelihood() const {
    assert(false);
    return 0.0f;
  }

}
