#ifdef TEST_WITH_VIDEO
# include <iostream>
# include <unistd.h>
# include <chrono>
# include <opencv2/core/core.hpp>
# include <opencv2/highgui/highgui.hpp>
# include <opencv2/video/tracking.hpp>
# include <opencv2/imgproc/imgproc.hpp>
# include "collision/collision.hh"
# include "peopledetect/peopledetectmanager.hh"
#else
# include "window.hh"
#endif

#ifdef TEST_WITH_VIDEO
void handleVideo(std::string path) {
  unsigned int fps = 30;
  collision::Collision collision(fps);
  peopledetect::PeopleDetectManager peopleDetect(fps);

  cv::VideoCapture cap;
  cap.open(path);

  if(!cap.isOpened()) {
    std::cout << "Could not initialize capturing...\n" << std::endl;
    return;
  }
  try {
    while(true) {
      cv::Mat frame;
      cap >> frame;
      if(frame.empty())
	break;
      Resource::getInstance().setMat(frame);
      //std::cout << "Set Image" << std::endl;
      //imshow("test", frame);
      //cv::waitKey(30);
      std::this_thread::sleep_for(std::chrono::milliseconds(1000 / fps));
    }
  } catch(std::exception const& e) {
    std::cout << "Exception : " << e.what() << std::endl;
  }
}
#endif

int main(int argc, char* argv[])
{
#ifdef TEST_WITH_VIDEO
  if (argc == 2) {
    // use for collision detections tests
    std::string path(argv[1]);
    handleVideo(path);
    return 0;
  } else {
    std::cout << "Invalid number of arguments" << std::endl;
    return 1;
  }
#else
  QApplication app(argc, argv);
  app.setOrganizationName("EPITA");
  app.setApplicationName("Kolibri");

  Window window(argc, argv);
  window.show();
  return app.exec();
#endif
}
