#ifndef ARDRONE_NAVDATA_HH
# define ARDRONE_NAVDATA_HH

# include <atomic>

namespace ardrone
{
  class NavData
  {
  public:
    static NavData& getInstance();

    void setBatteryLevel(unsigned battery_level);
    void setState(unsigned state);
    void setAltitude(int altitude);
    void setTheta(double theta);
    void setPhi(double phi);
    void setPsi(double psi);
    void setVx(double vx);
    void setVy(double vy);
    void setVz(double vz);

    void setRoll(double roll);
    void setPitch(double pitch);
    void setYaw(double yaw);
    void setHeight(double height);

    unsigned getBatteryLevel() const;
    unsigned getState() const;
    int getAltitude() const;
    double getTheta() const;
    double getPhi() const;
    double getPsi() const;
    double getVx() const;
    double getVy() const;
    double getVz() const;

    double getRoll() const;
    double getPitch() const;
    double getYaw() const;
    double getHeight() const;

  private:
    NavData();

    std::atomic<unsigned> _battery_level;
    std::atomic<unsigned> _state;
    std::atomic<int> _altitude;
    std::atomic<double> _theta;
    std::atomic<double> _phi;
    std::atomic<double> _psi;
    std::atomic<double> _vx;
    std::atomic<double> _vy;
    std::atomic<double> _vz;

    std::atomic<double> _roll;
    std::atomic<double> _pitch;
    std::atomic<double> _yaw;
    std::atomic<double> _height;
  };
}

#endif
