include_directories(
  ${OpenCV_INCLUDE_DIRS}
  )

add_library(stabilisation
  stabilisation.cc
  map.cc
  people.cc
  peopleDetect.cc
  Kalman.cc
  )

target_link_libraries(stabilisation PRIVATE
  ${OpenCV_LIBS}
  ${CMAKE_THREAD_LIBS_INIT}
  Qt5::Widgets
  )