#ifndef PEOPLEDETECT_HH
# define PEOPLEDETECT_HH

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

namespace peopleDetect {


  class PeopleDetect {
  public:
    PeopleDetect();
    ~PeopleDetect();
    void detectPeople(cv::Mat& src);





  private:
    void findPeople(cv::Mat& src, std::vector<cv::Rect>& found);
    void validateRect(cv::Rect& rect, const int width, const int height);
    float* getVector(cv::Mat& src);
    float* copyVector(float* vector);

    float getLikelyhood(float* vect1, float* vect2);
    // return index of closest one
    int getClosest(float** vects, const unsigned int size);

    long currentImage_;
    float* ref_;

  };

}


#endif /* PEOPLEDETECT_HH */
