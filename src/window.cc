#include "window.hh"

#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "ardrone/drone.hh"
#include "ardrone/video.hh"

# include "peopledetect/human.hh"
# include "peopledetect/HumanInfo.hh"
# include "peopledetect/HumanRTH.hh"
# include "peopledetect/PeopleDetectRTH.hh"

#include "settings-manager.hh"

Window::Window(int argc, char* argv[])
  : _droneThread(nullptr)
  , _stabilisationThread(nullptr)
  , _executionThread(nullptr)
  , _exitExecutionThread(false)
  , _collision(nullptr)
  , _people(nullptr)
  , _joystickHandler(nullptr)
  , _argc(argc)
  , _argv(argv)
  , _currentView(DEFAULT)
  , _highlightPeople(true)
{
  createActions();
  createMenus();
  createStatusBar();
  createToolBars();

  _bottomLayout = new QHBoxLayout;
  createControls();

  _frameLabel = new ui::VideoLabel;
  _frameLabel->setAlignment(Qt::AlignCenter);
  connect(_frameLabel, SIGNAL(clicked(int, int)), this, SLOT(handleFrameClick(int, int)));

  _videoLayout = new QHBoxLayout;
  _videoLayout->addWidget(_frameLabel);

  QGroupBox *frameLabelBox = new QGroupBox;
  frameLabelBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
  frameLabelBox->setLayout(_videoLayout);

  QVBoxLayout* topLayout = new QVBoxLayout;
  topLayout->addWidget(frameLabelBox);

  _mainLayout = new QVBoxLayout;
  _mainLayout->addLayout(topLayout);
  _mainLayout->addLayout(_bottomLayout);

  QWidget *mainWidget = new QWidget;
  mainWidget->setLayout(_mainLayout);

  setCentralWidget(mainWidget);
}

void Window::updateFrame()
{
  cv::Mat frame;

  if (_currentView == COLLISION && _collision != nullptr)
    _collision->copyDebugFrameTo(frame);
  else
    ardrone::Video::getInstance().copyLastFrameTo(frame, ardrone::Video::eagleEye::NONE);

  if (!frame.empty())
  {
    if (_highlightPeople)
      highlightPeople(frame);

    _frameLabel->update(frame);
  }
}

void Window::updateNavData()
{
  const ardrone::NavData& data = ardrone::NavData::getInstance();
  _batteryLabel->setText(QString::number(data.getBatteryLevel()));
  _altitudeLabel->setText(QString::number(data.getAltitude()));
  _thetaLabel->setText(QString::number(data.getTheta()));
  _phiLabel->setText(QString::number(data.getPhi()));
  _psiLabel->setText(QString::number(data.getPsi()));
  _vxLabel->setText(QString::number(data.getVx()));
  _vyLabel->setText(QString::number(data.getVy()));
  _vzLabel->setText(QString::number(data.getVz()));
}

void Window::updateExecutionTimes()
{
  unsigned ms;

  if (_stabilisationThread != nullptr)
  {
    _autoFlightState->setText(_stabilisationThread->getManualFlight() ? "inactive" : "active");
  }

  if (_collision != nullptr)
  {
    ms = _collision->getLastExecutionTime().count();
    _collisionTime->setText(QString::number(ms) + " ms");
  }

  if (_people != nullptr)
  {
    ms = _people->getLastExecutionTime().count();
    _peopleDetectTime->setText(QString::number(ms) + " ms");
  }
}

void Window::about()
{
  QMessageBox::about(this,
                     tr("About Kolibri"),
                     tr("<b>Kolibri</b> was developped by Karim Guiga, "
                        "Guillaume Klein and Romaric Thiam."));
}

void Window::exit()
{
  droneDisconnect();
}

void Window::droneConnect()
{
  if (_droneThread == nullptr)
  {
    _droneThread = new ardrone::Thread(_argc, _argv);
    _joystickHandler = new JoystickHandler();
    _connectAct->setEnabled(false);
    waitForConnection();
  }
}

void Window::droneDisconnect()
{
  if (_droneThread != nullptr && _droneThread->isConnected())
  {
    stop();
    stopDroneUpdate();

    delete _droneThread;
    _droneThread = nullptr;

    delete _joystickHandler;
    _joystickHandler = nullptr;

    _connectAct->setEnabled(true);
    _takeOffAct->setEnabled(false);
    _landAct->setEnabled(false);
    _startAct->setEnabled(false);
    _recordAct->setEnabled(false);
    _stopRecordAct->setEnabled(false);
  }
}

void Window::checkConnection()
{
  if (_droneThread->isConnected())
  {
    _connectionTimer->stop();
    delete _connectionTimer;

    ardrone::Drone::getInstance().init();
    startDroneUpdate();

    _connectAct->setEnabled(false);
    _takeOffAct->setEnabled(true);
    _landAct->setEnabled(true);
    _startAct->setEnabled(true);
    _recordAct->setEnabled(true);
  }
}

void Window::takeOff()
{
  ardrone::Drone::getInstance().takeOff();
}

void Window::land()
{
  ardrone::Drone::getInstance().land();
}

void Window::executionLoop()
{
  while (!_exitExecutionThread)
  {
    cv::Mat mat = Resource::getInstance().getCopyMat(false);

    std::thread collision(&collision::Collision::update, _collision, mat.clone());
    std::thread peopleDetect(&peopledetect::PeopleDetectHelper::update, _people, mat.clone());

    collision.join();
    peopleDetect.join();
  }
}

void Window::start()
{
  if (_executionThread == nullptr)
  {
    // Create people detect and collision objects
    _people = new peopledetect::PeopleDetectManager(ardrone::Video::framerate);
    //_people = new peopledetect::PeopleDetectRTH(ardrone::Video::framerate);

    _collision = new collision::Collision(ardrone::Video::framerate);

    // Launch threads
    _stabilisationThread = new stabilisation::Stabilisation(*_collision, *_people);
    _executionThread = new std::thread(&Window::executionLoop, this);

    _startAct->setEnabled(false);
    _stopAct->setEnabled(true);
    _collisionDebugAct->setEnabled(true);

    startExecutionTimesUpdate();
  }
}

void Window::stop()
{
  if (_executionThread != nullptr)
  {
    stopExecutionTimesUpdate();

    _startAct->setEnabled(true);
    _stopAct->setEnabled(false);
    _cancelTargetAct->setEnabled(false);
    _defaultVideoAct->setChecked(true);
    _collisionDebugAct->setEnabled(false);

    // Stop stabilisation
    delete _stabilisationThread;
    _stabilisationThread = nullptr;

    // Stop execution thread
    _exitExecutionThread = true;
    _executionThread->join();
    _exitExecutionThread = false;
    delete _executionThread;
    _executionThread = nullptr;

    // Reset people detect
    delete _people;
    _people = nullptr;

    // Reset collision
    delete _collision;
    _collision = nullptr;
  }
}

void Window::cancelTarget()
{
  stop();
  start();
}

void Window::startRecording()
{
  ardrone::Video::getInstance().startRecording();
  _stopRecordAct->setEnabled(true);
  _recordAct->setEnabled(false);
}

void Window::stopRecording()
{
  ardrone::Video::getInstance().stopRecording();
  _stopRecordAct->setEnabled(false);
  _recordAct->setEnabled(true);
}

void Window::updateAltitude(int altitude)
{
  ardrone::Drone::getInstance().setAbsoluteAltitude(altitude);
}

void Window::handleFrameClick(int x, int y)
{
  if (_people != nullptr) {
    peopledetect::HumanInfo* human = _people->setNewTarget(x,y);
    if(human != nullptr) {
      _cancelTargetAct->setEnabled(true);
      _collision->setNewTarget(human);
      delete(human);
    } else {
      _cancelTargetAct->setEnabled(false);
    }
  }
}

void Window::showDefaultVideo()
{
  _currentView = DEFAULT;
}

void Window::showCollisionDebug()
{
  _currentView = COLLISION;
}

void Window::togglePeopleHighlighting()
{
  _highlightPeople = !_highlightPeople;
}

void Window::toggleGPU()
{
  SettingsManager::getInstance().toggleGPU();
}

void Window::closeEvent(QCloseEvent *bar)
{
  exit();
  bar->accept();
}

void Window::highlightPeople(cv::Mat& mat)
{
  if (_people == nullptr)
    return;

  //draw all humans
  std::vector<peopledetect::HumanInfo*>* people = _people->getCopyHumans();

  if (people != nullptr)
  {
    for (const peopledetect::HumanInfo* human: *people)
    {
      const cv::Rect& r = human->getRectangle();
      /*
        r.x += cvRound(r.width*0.1);
        r.width = cvRound(r.width*0.8);
        r.y += cvRound(r.height*0.07);
        r.height = cvRound(r.height*0.8);
      */

      cv::rectangle(mat, r.tl(), r.br(), cv::Scalar(0, 255, 0), 2);
    }

    {
      const unsigned int size = people->size();
      for(unsigned int i = 0; i < size; i++) {
	delete((*people)[i]);
      }
    }
    delete people;
  }

  // print points
  const std::vector<cv::Point2f>*  points = 
    _collision->getCopyPointsToTrack();
  for(unsigned int i = 0; i < points->size(); i++) {
    circle(mat, (*points)[i], 3, cv::Scalar(0, 0, 255), -1, 8);
  }
  delete(points);

  //draw current Target
  cv::Rect* target = _stabilisationThread->getTargetCopy();

  if (target != nullptr)
  {
    const cv::Rect& r = *target;
    cv::rectangle(mat, r.tl(), r.br(), cv::Scalar(0, 255, 255), 3);
    delete (target);
  }
}

void Window::createActions()
{
  _exitAct = new QAction(tr("E&xit"), this);
  _exitAct->setShortcuts(QKeySequence::Quit);
  _exitAct->setStatusTip(tr("Exit the application"));
  connect(_exitAct, SIGNAL(triggered()), this, SLOT(close()));

  _connectAct = new QAction(tr("&Connect"), this);
  _connectAct->setStatusTip(tr("Connect to the drone"));
  connect(_connectAct, SIGNAL(triggered()), this, SLOT(droneConnect()));

  _takeOffAct = new QAction(tr("Take Off"), this);
  _takeOffAct->setStatusTip(tr("Take the drone off"));
  _takeOffAct->setEnabled(false);
  connect(_takeOffAct, SIGNAL(triggered()), this, SLOT(takeOff()));

  _landAct = new QAction(tr("Land"), this);
  _landAct->setStatusTip(tr("Land the drone"));
  _landAct->setEnabled(false);
  connect(_landAct, SIGNAL(triggered()), this, SLOT(land()));

  _aboutAct = new QAction(tr("&About"), this);
  _aboutAct->setStatusTip(tr("Show the application's About box"));
  connect(_aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  _startAct = new QAction(tr("Start"), this);
  _startAct->setStatusTip(tr("Start"));
  _startAct->setEnabled(false);
  connect(_startAct, SIGNAL(triggered()), this, SLOT(start()));

  _stopAct = new QAction(tr("Stop"), this);
  _stopAct->setStatusTip(tr("Stop"));
  _stopAct->setEnabled(false);
  connect(_stopAct, SIGNAL(triggered()), this, SLOT(stop()));

  _cancelTargetAct = new QAction(tr("Cancel target"), this);
  _cancelTargetAct->setStatusTip(tr("Cancel the current target"));
  _cancelTargetAct->setEnabled(false);
  connect(_cancelTargetAct, SIGNAL(triggered()), this, SLOT(cancelTarget()));

  _recordAct = new QAction(tr("Start recording"), this);
  _recordAct->setStatusTip(tr("Start video recording"));
  _recordAct->setEnabled(false);
  connect(_recordAct, SIGNAL(triggered()), this, SLOT(startRecording()));

  _stopRecordAct = new QAction(tr("Stop recording"), this);
  _stopRecordAct->setStatusTip(tr("Stop video recording"));
  _stopRecordAct->setEnabled(false);
  connect(_stopRecordAct, SIGNAL(triggered()), this, SLOT(stopRecording()));

  _defaultVideoAct = new QAction(tr("Default"), this);
  _defaultVideoAct->setCheckable(true);
  _defaultVideoAct->setStatusTip(tr("Display the default video stream"));
  connect(_defaultVideoAct, SIGNAL(triggered()), this, SLOT(showDefaultVideo()));

  _collisionDebugAct = new QAction(tr("Collision debug"), this);
  _collisionDebugAct->setCheckable(true);
  _collisionDebugAct->setStatusTip(tr("Display the collision debug stream"));
  _collisionDebugAct->setEnabled(false);
  connect(_collisionDebugAct, SIGNAL(triggered()), this, SLOT(showCollisionDebug()));

  _highlightPeopleAct = new QAction(tr("Highlight detected people"), this);
  _highlightPeopleAct->setCheckable(true);
  _highlightPeopleAct->setChecked(_highlightPeople);
  _highlightPeopleAct->setStatusTip(tr("Highlight detected people on the video stream"));
  connect(_highlightPeopleAct, SIGNAL(triggered()), this, SLOT(togglePeopleHighlighting()));

  _gpuToggleAct = new QAction(tr("Enable GPU"), this);
  _gpuToggleAct->setCheckable(true);
  _gpuToggleAct->setChecked(SettingsManager::getInstance().isGPUEnabled());
  _gpuToggleAct->setStatusTip(tr("Enable or disable GPU support"));
  connect(_gpuToggleAct, SIGNAL(triggered()), this, SLOT(toggleGPU()));

  _videoView = new QActionGroup(this);
  _videoView->addAction(_defaultVideoAct);
  _videoView->addAction(_collisionDebugAct);
  _defaultVideoAct->setChecked(true);
}

void Window::createMenus()
{
  _fileMenu = menuBar()->addMenu(tr("&File"));
  _fileMenu->addAction(_exitAct);

  menuBar()->addSeparator();

  _viewMenu = menuBar()->addMenu(tr("&View"));
  _viewMenu->addSeparator()->setText(tr("Video"));
  _viewMenu->addAction(_defaultVideoAct);
  _viewMenu->addAction(_collisionDebugAct);
  _viewMenu->addSeparator();
  _viewMenu->addAction(_highlightPeopleAct);

  menuBar()->addSeparator();

  _settingsMenu = menuBar()->addMenu(tr("&Settings"));
  _settingsMenu->addAction(_gpuToggleAct);

  menuBar()->addSeparator();

  _helpMenu = menuBar()->addMenu(tr("&Help"));
  _helpMenu->addAction(_aboutAct);
}

void Window::createStatusBar()
{
  statusBar()->showMessage(tr("Ready"));
}

void Window::createToolBars()
{
  _droneToolBar = addToolBar(tr("Drone"));
  _droneToolBar->addAction(_connectAct);
  _droneToolBar->addAction(_takeOffAct);
  _droneToolBar->addAction(_landAct);

  _controlToolBar = addToolBar(tr("Control"));
  _controlToolBar->addAction(_startAct);
  _controlToolBar->addAction(_stopAct);
  _controlToolBar->addAction(_cancelTargetAct);

  _recordToolBar = addToolBar(tr("Record"));
  _recordToolBar->addAction(_recordAct);
  _recordToolBar->addAction(_stopRecordAct);
}

void Window::createTextField(const std::string& name,
                             const std::string& defaultValue,
                             QLabel*& label,
                             QBoxLayout* parent)
{
  std::string nameFormat = name + ": ";
  QLabel *labelName = new QLabel(tr(nameFormat.c_str()));
  label = new QLabel(tr(defaultValue.c_str()));

  QHBoxLayout *entryLayout = new QHBoxLayout;
  entryLayout->addWidget(labelName);
  entryLayout->addWidget(label);

  parent->addLayout(entryLayout);
}

void Window::createControls()
{
  QLabel *altitudeLabel = new QLabel(tr("Altitude:"));
  _altitudeControl = new QSpinBox;
  _altitudeControl->setRange(ardrone::Drone::minAltitude, ardrone::Drone::maxAltitude);
  _altitudeControl->setSingleStep(ardrone::Drone::altitudeStep);
  _altitudeControl->setValue(ardrone::Drone::defaultAltitude);
  _altitudeControl->setSuffix(ardrone::Drone::altitudeUnit);
  _altitudeControl->setFocusPolicy(Qt::NoFocus);
  connect(_altitudeControl, SIGNAL(valueChanged(int)), this, SLOT(updateAltitude(int)));

  QHBoxLayout *altitudeLayout = new QHBoxLayout;
  altitudeLayout->addWidget(altitudeLabel);
  altitudeLayout->addWidget(_altitudeControl);

  QVBoxLayout *controlLayout = new QVBoxLayout;
  controlLayout->setAlignment(Qt::AlignTop);
  controlLayout->addLayout(altitudeLayout);

  QGroupBox *controlBox = new QGroupBox(tr("Controls"));
  controlBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
  controlBox->setLayout(controlLayout);

  QVBoxLayout *navDataLayout = new QVBoxLayout;
  navDataLayout->setAlignment(Qt::AlignTop);
  createTextField("Battery", "not available", _batteryLabel, navDataLayout);
  createTextField("Altitude", "not available", _altitudeLabel, navDataLayout);
  createTextField("Theta", "not available", _thetaLabel, navDataLayout);
  createTextField("Phi", "not available", _phiLabel, navDataLayout);
  createTextField("Psi", "not available", _psiLabel, navDataLayout);
  createTextField("Vx", "not available", _vxLabel, navDataLayout);
  createTextField("Vy", "not available", _vyLabel, navDataLayout);
  createTextField("Vz", "not available", _vzLabel, navDataLayout);

  QGroupBox *navDataBox = new QGroupBox(tr("NavData"));
  navDataBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
  navDataBox->setLayout(navDataLayout);

  QVBoxLayout *executionTimesLayout = new QVBoxLayout;
  executionTimesLayout->setAlignment(Qt::AlignTop);
  createTextField("Automatic flight", "inactive", _autoFlightState, executionTimesLayout);
  createTextField("People detection time", "not running", _peopleDetectTime, executionTimesLayout);
  createTextField("Stabilisation time", "not running", _collisionTime, executionTimesLayout);

  QGroupBox *executionTimesBox = new QGroupBox(tr("Monitoring"));
  executionTimesBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
  executionTimesBox->setLayout(executionTimesLayout);

  _bottomLayout->addWidget(controlBox);
  _bottomLayout->addWidget(navDataBox);
  _bottomLayout->addWidget(executionTimesBox);
}

void Window::waitForConnection()
{
  _connectionTimer = new QTimer;
  connect(_connectionTimer, SIGNAL(timeout()), this, SLOT(checkConnection()));
  _connectionTimer->start(20);
}

void Window::startDroneUpdate()
{
  _timer = new QTimer;
  connect(_timer, SIGNAL(timeout()), this, SLOT(updateFrame()));
  connect(_timer, SIGNAL(timeout()), this, SLOT(updateNavData()));
  _timer->start(30);
}

void Window::stopDroneUpdate()
{
  _timer->stop();
  delete _timer;

  _frameLabel->clear();
  _batteryLabel->setText("not available");
  _altitudeLabel->setText("not available");
  _thetaLabel->setText("not available");
  _phiLabel->setText("not available");
  _psiLabel->setText("not available");
  _vxLabel->setText("not available");
  _vyLabel->setText("not available");
  _vzLabel->setText("not available");
}

void Window::startExecutionTimesUpdate()
{
  _executionTimesTimer = new QTimer;
  connect(_executionTimesTimer, SIGNAL(timeout()), this, SLOT(updateExecutionTimes()));
  _executionTimesTimer->start(20);
}

void Window::stopExecutionTimesUpdate()
{
  _executionTimesTimer->stop();
  delete _executionTimesTimer;

  _autoFlightState->setText("inactive");
  _collisionTime->setText("not running");
  _peopleDetectTime->setText("not running");
}


void Window::keyPressEvent(QKeyEvent *event)
{
  bool manualControl = false;

  if(event->key() == Qt::Key::Key_S) {
    manualControl = true;
    ardrone::Drone::getInstance().moveBackward(1.0f);
  } else if (event->key() == Qt::Key::Key_Z) {
    manualControl = true;
    ardrone::Drone::getInstance().moveForward(1.0f);
  }

  if(event->key() == Qt::Key::Key_D) {
    manualControl = true;
    ardrone::Drone::getInstance().moveRight(1.0f);
  } else if (event->key() == Qt::Key::Key_Q) {
    manualControl = true;
    ardrone::Drone::getInstance().moveLeft(1.0f);
  }

  if(event->key() == Qt::Key::Key_E) {
    manualControl = true;
    ardrone::Drone::getInstance().turnRight(1.0f);
  } else if (event->key() == Qt::Key::Key_A) {
    manualControl = true;
    ardrone::Drone::getInstance().turnLeft(1.0f);
  }

  if(event->key() == Qt::Key::Key_Space) {
    manualControl = true;
    ardrone::Drone::getInstance().land();
  }

  if (event->key() == Qt::Key::Key_H) {
    ardrone::Drone::getInstance().increaseAltitude();
    _altitudeControl->setValue(_altitudeControl->value() + ardrone::Drone::altitudeStep);
  } else if (event->key() == Qt::Key::Key_L) {
    ardrone::Drone::getInstance().decreaseAltitude();
    _altitudeControl->setValue(_altitudeControl->value() - ardrone::Drone::altitudeStep);
  }

  if(event->key() == Qt::Key::Key_P) {
    manualControl = true;
    ardrone::Drone::getInstance().takeOff();
  }

  if (_stabilisationThread != nullptr) {
    if(event->key() == Qt::Key::Key_O) {
      _stabilisationThread->setManualFlight(false);
    } else if (manualControl) {
      _stabilisationThread->setManualFlight(true);
    }
  }
}

void Window::keyReleaseEvent(QKeyEvent *event)
{
  if (event->key() == Qt::Key::Key_S || event->key() == Qt::Key::Key_Z)
    ardrone::Drone::getInstance().stopMovingHorizontally();

  if (event->key() == Qt::Key::Key_D || event->key() == Qt::Key::Key_Q)
    ardrone::Drone::getInstance().stopMovingLaterally();

  if (event->key() == Qt::Key::Key_E || event->key() == Qt::Key::Key_A)
    ardrone::Drone::getInstance().stopTurning();
}
