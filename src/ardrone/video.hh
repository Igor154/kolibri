#ifndef ARDRONE_VIDEO_HH
# define ARDRONE_VIDEO_HH

# include <opencv2/core/core.hpp>
# include <opencv2/highgui/highgui.hpp>
# include <opencv2/imgproc/imgproc.hpp>

# include <mutex>
# include <atomic>

namespace ardrone
{
  class Video
  {
  public:
    static const unsigned int framerate = 30;

    enum eagleEye
    {
      NONE = 0,
      LATERAL = 1,
      FULL = 2
    };

    static Video& getInstance();

    size_t getWidth() const;
    size_t getHeight() const;

    void updateFrame(void* data, size_t width, size_t height, double phi, double theta);

    cv::Mat cloneLastFrame(enum eagleEye stabilisation) const;
    void copyLastFrameTo(cv::Mat& mat, enum eagleEye stabilisation) const;

    void startRecording();
    void stopRecording();
    bool isRecording() const;

  private:
    Video();

    mutable std::mutex _mutex;
    cv::Mat _current;
    double _currentTheta;
    double _currentPhi;

    std::atomic<bool> _record;
  };
}

#endif
