#include "Kalman.hh"



namespace stabilisation {

  TargetPosition::TargetPosition(const time_point timePoint,
                                 const bool isPrediction,
				 const double posX,
				 const double posY):
    timePoint_(timePoint),
    isPrediction_(isPrediction),
    posX_(posX),
    posY_(posY) {
    
  }

  time_point TargetPosition::getTimePoint() const {
    return timePoint_;
  }

  bool TargetPosition::getIsPrediction() const {
    return isPrediction_;
  }
  
  double TargetPosition::getPosX() const {
    return posX_;
  }

  double TargetPosition::getPosY() const {
    return posY_;
  }




  Kalman::Kalman(): dronePosition_(0.0d, 0.0d),
		    expectedPosition_(0.0d, 0.0d),
		    expectedSpeed_(0.0d, 0.0d) {
    
  }

  Kalman::~Kalman() {

  }


  time_point Kalman::timeNow() const {
    time_point t = std::chrono::high_resolution_clock::now();
    return t;
  }

  double Kalman::elapsedTime(const time_point& start, const time_point& end) const {
    typedef std::chrono::duration<double> duration;
    const duration time_span = std::chrono::duration_cast<duration>(end - start);
    return time_span.count();
  }
  

  double Kalman::degreeToRadian(const double angle) {
    const double value = (angle / 180000.0f) * PI;
    assert(value >= -PI && value <= PI);
    return value;
  }

  void Kalman::update(const ardrone::NavData& data) {
    removeOldTargetPosition();
    updateDronePosition(data);
    updateExpectedPosition();

  }

  // remove old and useless datas
  void Kalman::removeOldTargetPosition() {
    const time_point now = timeNow();
    if(targetPositions_.size() > 0) {
      while(targetPositions_.size() > 0
	    && elapsedTime(targetPositions_.back().getTimePoint(), now) > MAX_TIME_POSITION) {
	assert(elapsedTime(targetPositions_.back().getTimePoint(), now) >= 0.0d);
	targetPositions_.pop_back();
      }
    }
  }

  void Kalman::updateDronePosition(const ardrone::NavData& data) {
    const double posX = dronePosition_.first;
    const double posY = dronePosition_.second;
    const double angle = degreeToRadian(data.getPsi());
    const double vx = (data.getVx() * cos(angle)) + (-data.getVy() * cos(angle + HALF_PI));
    const double vy = (data.getVx() * sin(angle)) + (-data.getVy() * sin(angle + HALF_PI));
    dronePosition_.first = posX + vx;
    dronePosition_.second = posY + vy;
    //std::cout << "drone vX : " << data.getVx() << " vY : " << data.getVy() << std::endl;
    //std::cout << "drone : " << posX << " " << posY << std::endl;
  }

  void Kalman::recalibrate(const double distanceToTarget,
			   const double angleToTarget,
			   const ardrone::NavData& data) {
    const double droneAngle = degreeToRadian(data.getPsi());
    const double posX = (distanceToTarget * cos(-angleToTarget + droneAngle)) + dronePosition_.first;
    const double posY = (distanceToTarget * sin(-angleToTarget + droneAngle)) + dronePosition_.second;
    targetPositions_.push_front(TargetPosition(timeNow(), false, posX, posY));    
    updateExpectedSpeed();
    resetExpectedPosition();
  }

  void Kalman::notCalibrate() {
    targetPositions_.push_front(TargetPosition(timeNow(), true, expectedPosition_.first, expectedPosition_.second));
  }
  
  double Kalman::angleToTarget() {
    return atan2(expectedPosition_.second - dronePosition_.second,
		 expectedPosition_.first - dronePosition_.first);
  }

  double Kalman::isTarget(const double distanceToTarget,
			  const double angleToTarget,
			  const ardrone::NavData& data) {
    const double droneAngle = degreeToRadian(data.getPsi());
    const double posX = distanceToTarget * cos(-angleToTarget + droneAngle) + dronePosition_.first;
    const double posY = distanceToTarget * sin(-angleToTarget + droneAngle) + dronePosition_.second;
    const double distance = getDistance(expectedPosition_.first, expectedPosition_.second,
				        posX, posY);
    return distance;
  }

  double Kalman::getDistance(const double x1,
			     const double y1,
			     const double x2,
			     const double y2) {
    const double dx = x2 - x1;
    const double dy = y2 - y1;
    const double sum = (dx * dx) + (dy * dy);
    return sqrt(sum);
  }


  void Kalman::resetExpectedPosition() {
    if(targetPositions_.size() > 8) {
      double sumX = 0.0d;
      double sumY = 0.0d;
      double total = 0.0d;
      //for(unsigned int i = targetPositions_.size() - 6; i < targetPositions_.size(); i++) {
      for(unsigned int i = 0; i < 8; i++) {
	const TargetPosition& current = targetPositions_[i];
	sumX = sumX + current.getPosX();
	sumY = sumY + current.getPosY();
	total = total + 1.0d;
      }
      double stockX = sumX / total;
      double stockY = sumY / total;
      for(unsigned int i = 0; i < 4; i++) {
        stockX = stockX + expectedSpeed_.first;
        stockY = stockY + expectedSpeed_.second;
      }
      expectedPosition_.first = stockX;
      expectedPosition_.second = stockY;
    }
  }

  void Kalman::updateExpectedPosition() {
    if(targetPositions_.size() >= 10) {
      const double posX = expectedPosition_.first;
      const double posY = expectedPosition_.second;
      const double vX = expectedSpeed_.first;
      const double vY = expectedSpeed_.second;
      expectedPosition_.first = posX + vX;
      expectedPosition_.second = posY + vY;
      //std::cout << "expectedPosition : " << expectedPosition_.first << " " << expectedPosition_.second << std::endl;
    }
  }

  void Kalman::updateExpectedSpeed() {
    if(targetPositions_.size() >= 10) {
      double coef = 1.0d;
      double sumCoef = 0.0d;
      double dx = 0.0d;
      double dy = 0.0d;
      for(unsigned int i = 0; i < targetPositions_.size() - 1; i++) {
	const TargetPosition& end = targetPositions_[i];
	const TargetPosition& start = targetPositions_[i + 1];
        double currentCoef = coef;
        if(end.getIsPrediction() || start.getIsPrediction()) {
          currentCoef = currentCoef * 0.7;
        }
	dx = dx + ((end.getPosX() - start.getPosX()) * currentCoef);
	dy = dy + ((end.getPosY() - start.getPosY()) * currentCoef);
	sumCoef = sumCoef + currentCoef;
	coef = coef * 0.90d;
      }
      dx = dx / sumCoef;
      dy = dy / sumCoef;
      expectedSpeed_.first = dx;
      expectedSpeed_.second = dy;
    }
  }
}
