#ifndef ARDRONE_THREAD_HH
# define ARDRONE_THREAD_HH

# include <thread>

namespace ardrone
{
  class Thread
  {
  public:
    Thread(int argc, char* argv[]);
    ~Thread();

    bool isConnected() const;

  private:
    void run();

    int _argc;
    char** _argv;

    std::thread _thread;
  };
}

#endif
