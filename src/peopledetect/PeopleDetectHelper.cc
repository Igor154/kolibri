#include "PeopleDetectHelper.hh"

namespace peopledetect {

  PeopleDetectHelper::~PeopleDetectHelper() {

  }


  void PeopleDetectHelper::update(cv::Mat mat) {
    assert(false);
  }
  
  std::vector<HumanInfo*>* PeopleDetectHelper::getCopyHumans() {
    assert(false);
    return nullptr;
  }
  
  void PeopleDetectHelper::recalibrateTarget(const HumanInfo& h) {
    assert(false);
  }

  std::chrono::milliseconds PeopleDetectHelper::getLastExecutionTime() const {
    assert(false);
  }  

  void PeopleDetectHelper::removeTarget() {
    assert(false);
  }

  HumanInfo* PeopleDetectHelper::setNewTarget(int X, int Y) {
    return nullptr;
  }

  float PeopleDetectHelper::getThreshold() {
    assert(false);
    return 1.0f;
  }
}
