#ifndef TOOLS_TIMER_HH
# define TOOLS_TIMER_HH

# include <chrono>
# include <ostream>

namespace tools
{
  template <typename T>
  class Timer;

  template <typename T>
  std::ostream& operator<<(std::ostream&, const Timer<T>&);

  template <typename T>
  class Timer
  {
  public:
    Timer();

    void reset();
    T getElapsedTime() const;

    friend std::ostream& operator<< <>(std::ostream& os, const Timer<T>& t);

  private:
    std::chrono::high_resolution_clock::time_point _start;
  };
}

# include "timer.hxx"

#endif /* !TOOLS_TIMER_HH */
