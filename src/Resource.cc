#include "Resource.hh"

#ifndef TEST_WITH_VIDEO
# include "ardrone/video.hh"
#endif

Resource& Resource::getInstance()
{
  static Resource resource;
  return resource;
}

cv::Mat Resource::getCopyMat(bool forCollision) const {
#ifdef TEST_WITH_VIDEO
  std::lock_guard<std::mutex> lock(mutex);
  return currentMat.clone();
#else
  return ardrone::Video::getInstance().cloneLastFrame(/*forCollision*/false
                                                      ? ardrone::Video::eagleEye::FULL
                                                      : ardrone::Video::eagleEye::LATERAL);
#endif
}

size_t Resource::getWidth() const
{
#ifdef TEST_WITH_VIDEO
  std::lock_guard<std::mutex> lock(mutex);
  return currentMat.cols;
#else
  return ardrone::Video::getInstance().getWidth();
#endif
}

size_t Resource::getHeight() const
{
#ifdef TEST_WITH_VIDEO
  std::lock_guard<std::mutex> lock(mutex);
  return currentMat.rows;
#else
  return ardrone::Video::getInstance().getHeight();
#endif
}

#ifdef TEST_WITH_VIDEO
void Resource::setMat(const cv::Mat& mat) {
  std::lock_guard<std::mutex> lock(mutex);
  mat.copyTo(currentMat);
}
#endif
