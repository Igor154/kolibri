#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <cmath>

#include "stabilisation.hh"
#include "../ardrone/navdata.hh"
#include "../peopledetect/human.hh"
#include "../Debug.hh"

namespace stabilisation {

  Stabilisation::Stabilisation(collision::Collision& collision,
			       peopledetect::PeopleDetectHelper& people):
    controller_(&Stabilisation::run, this),
    manualFlight_(true),
    exit_(false),
    collision_(collision),
    people_(people),
    humanTarget(nullptr) {
    targetDistance_ = 0;
    targetAngle_ = angleFloatToInt(ardrone::NavData::getInstance().getPsi());
    kalmanCounter_ = 0;
  }

  Stabilisation::~Stabilisation() {
    exit_ = true;
    controller_.join();
  }

  void Stabilisation::stabiliseVY(const ardrone::NavData& data, const int targetSpeed) {
    const int vy = (int)data.getVy();
    //std::cout << "vy " << vy << std::endl;
    int correction = targetSpeed - vy;
    correction = correction / 3;
    if(correction > 1000) {
      correction = 1000;
    }
    if(correction < -1000) {
      correction = -1000;
    }
    correction = (float)(((int)correction));
    const float convert = (float)correction;
    //std::cout << "stabiliseVY " << (convert / 1000.0f) << std::endl;
    ardrone::Drone::getInstance().moveRight(convert / 1000.0f);
  }

  /*
   * have a min/max bound
   */
  void Stabilisation::stabiliseVX(const ardrone::NavData& data, int targetSpeed) {
    const int vx = (int)data.getVx();
    targetSpeed = targetSpeed / 10;
    int correction = targetSpeed - vx;
    //correction = correction / 50;
    correction = correction / 36;
    if(correction > 1000) {
      correction = 1000;
    }
    if(correction < -1000) {
      correction = -1000;
    }
    //correction = correction / 2;
    const float convert = (float)correction;
    ardrone::Drone::getInstance().moveForward(convert / 1000.0f);
  }
  
  void Stabilisation::normalCruise(const ardrone::NavData& data) {
    stabiliseVX(data, 1000);
    //stabiliseVY(data, 0);
    if(collision_.mustStop()) {
      collision_.resetDanger();
    }
  }

  int Stabilisation::angleFloatToInt(float angle) {
    assert(angle <= 180000.0f && angle >= -180000.0f);
    angle = angle / 1000.0f;
    int newAngle = (int)angle;
    assert(newAngle >= -180 && newAngle <= 180);
    if(newAngle < 0) {
      newAngle = newAngle + 360;
    }
    assert(newAngle >= 0 && newAngle < 360);
    return newAngle;
  }

  int Stabilisation::angleAdd(int angle1, int angle2) {
    int ret = angle1 + angle2;
    while(ret < 0) {
      ret = ret + 360;
    }
    while(ret >= 360) {
      ret = ret - 360;
    }
    assert(ret >= 0);
    assert(ret < 360);
    return ret;
  }

  int Stabilisation::angleSub(int angle1, int angle2) {
    return angleAdd(angle1, -angle2);
  }

  int getDist(int currentAngle, int bestAngle) {
    int ret = currentAngle - bestAngle;
    if(ret < 0) {
      ret = -ret;
    }
    return ret;
  }

  void Stabilisation::pilotageAuto() {
    if(true) {
      followPeople();
    } else {
      // nothing
      //ardrone::NavData& data = ardrone::NavData::getInstance();
    }
  }

  // private
  void Stabilisation::run() {
    std::chrono::duration<int, std::milli> waitingTime(WAITING_TIME);
    while(!exit_) {
      std::this_thread::sleep_for(waitingTime);
      ardrone::NavData& data = ardrone::NavData::getInstance();
      kalman_.update(data);
      if(!manualFlight_) {
	pilotageAuto();
      }
    }
  }

  bool Stabilisation::getManualFlight() const {
    return manualFlight_;
  }

  void Stabilisation::setManualFlight(bool newValue) {
      manualFlight_ = newValue;
  }

  void Stabilisation::stabiliseAngle(const ardrone::NavData& data) {
    const int currentAngle = angleFloatToInt(data.getPsi());
    assert(currentAngle >= 0);
    assert(currentAngle < 360);
    assert(targetAngle_ >= 0);
    assert(targetAngle_ < 360);

    const int left = angleSub(currentAngle, targetAngle_);
    const int right = angleSub(targetAngle_, currentAngle);

    int turnRight;
    if(left < right) {
      // turn left
      turnRight = -left;
    } else {
      // turn right
      turnRight = right;
    }
    
    float turn = ((double)turnRight / 50.0f);
    //std::cout << "turn : " << turn << std::endl;
    if(turn < -1.0d) {
      turn = -1.0d;
    } else if(turn > 1.0d) {
      turn = 1.0d;
    }
    assert(turn >= -1.0d);
    assert(turn <= 1.0d);
    ardrone::Drone::getInstance().turnRight(turn);
  }


  void Stabilisation::followMovements(ardrone::NavData& data) {
    const int frequency = 1000 / WAITING_TIME;
    assert(frequency > 0);

    // handle up/back
    stabiliseVX(data, targetDistance_);
    stabiliseVY(data, 0);
    //stabiliseVY(data, targetDistance_);
    targetDistance_ = targetDistance_ - data.getVx();

    // handle left/right
    stabiliseAngle(data);
  }

  /* Used by run() if manualFlight_ == false
   *
   */
  void Stabilisation::followPeople() {
    // handle collision avoidance
    ardrone::NavData& data = ardrone::NavData::getInstance();
    followMovements(data);

    std::vector<peopledetect::HumanInfo*>* humanList = people_.getCopyHumans();
    const std::vector<cv::Point2f>* points = collision_.getCopyPointsToTrack();
    const unsigned int size = humanList->size();

    if(size >= 1) {
	int index = -1;
        if(points->size() >= 3) {
	  // compute likelyhood with points
          float* likelyhoods = new float[size];
          for(unsigned int i = 0; i < size; i++) {
            likelyhoods[i] = computeLikelyhoodHuman(*(*humanList)[i], points);
          }

	  // use the previous likelyhood to choose a target
          {
            float bestScore = 0.0f;
            for(unsigned int i = 0; i < size; i++) {
              const float currentLike = likelyhoods[i];
              if(currentLike > MINIMAL_THRESHOLD
                 && currentLike > bestScore) {
                bestScore = currentLike;
                index = i;
              }
            }
          }
          delete[](likelyhoods);
        }
        
	if(points->size() <= 10) {
	  float bestScore = -1.0f;
	  if(index < 0) {
	    // didn't find the person with the points,
	    // use human likehood
	    for(unsigned int i = 0; i < size; i++) {
	      const float value = (*humanList)[i]->getLikelihood();
	      if(value > bestScore && value > people_.getThreshold()) {
	       	bestScore = value;
	       	index = i;
	      }
	    }
	  }
	}

	// try to use kalman to find the target
	if(index == -1) {
	  double minDistance = 1000000000.0d;
	  for(unsigned int i = 0; i < size; i++) {
	    const peopledetect::HumanInfo* currentHuman = (*humanList)[i];
	    const cv::Rect rectangle = currentHuman->getRectangle();
	    const double distance = kalman_.isTarget(estimateDistanceToTarget(rectangle) + Kalman::BASE_DISTANCE,
						     estimateAngleToTarget(rectangle),
						     data);
	    if(distance < minDistance && distance < Kalman::MIN_DISTANCE) {
	      //std::cout << "Kalman approuved !" << std::endl;
	      minDistance = distance;
	      index = i;
	    }
	  }
	}

	// set new target / found a target
	if(index >= 0) {
	  const peopledetect::HumanInfo* bestHuman = (*humanList)[index];
	  const cv::Rect rectangle = bestHuman->getRectangle();
	  targetAnglePoint(rectangle);
	  setTarget(rectangle);
	  // re calibrate
	  //std::cout << "like : " << bestHuman->getLikelihood() << std::endl;
	  //people_.recalibrateTarget(*bestHuman);
	  collision_.setNewTarget(bestHuman);
	  kalman_.recalibrate(estimateDistanceToTarget(rectangle) + Kalman::BASE_DISTANCE,
			      estimateAngleToTarget(rectangle),
			      data);
	  setDistanceToTarget(bestHuman->getRectangle());
          
	} else {
          kalmanCounter_++;
          kalman_.notCalibrate();
          if(kalmanCounter_ < 10) {
            //// try to use points
            followPoints(points);
          } else {
            // try to use Kalman
            followKalman();
          }
	}

    } else { // humanList->size() == 0
      kalmanCounter_++;
      if(kalmanCounter_ < 10) {
        //// try to use points
        followPoints(points);
      } else {
        // try to use Kalman
        followKalman();
      }
    }

    for(unsigned int i = 0; i < size; i++) {
      delete((*humanList)[i]);
    }
    delete(humanList);
    delete(points);
  }

  void Stabilisation::followKalman() {
    const double angleKalman = kalman_.angleToTarget();
    //std::cout << "angle degree : " << ((angleKalman / Kalman::PI) * 180.0) << std::endl;
    //std::cout << "angle degree drone : " << (ardrone::NavData::getInstance().getPsi() / 1000.0) << std::endl;
    const int angleKalmanInt = angleAdd((int)((angleKalman / Kalman::PI) * 180.0d), 0);
    const int currentAngle = angleFloatToInt(ardrone::NavData::getInstance().getPsi());
    //std::cout << "Dangle : " << (angleKalmanInt - currentAngle) << std::endl;
    //targetAngle_ = angleAdd(currentAngle, angleAdd(angleKalmanInt, 90));
    targetAngle_ = angleKalmanInt;
    targetDistance_ = 0.0d;
  }

  void Stabilisation::followPoints(const std::vector<cv::Point2f>* points) {
    assert(points != nullptr);

    if(points->size() >= MINIMAL_NUMBER_POINTS_FOLLOW) {
      assert(points->size() >= 1);
      float averageX = 0;
      float averageY = 0;
      for(unsigned int i = 0; i < points->size(); i++) {
	averageX = averageX + (*points)[i].x;
	averageY = averageY + (*points)[i].y;
      }
      averageX = averageX / (float)points->size();
      averageY = averageY / (float)points->size();
      targetAnglePoint(averageX/*, averageY*/);
      setDistanceToTarget(points);
    } else {
      // drone don't move
    }
  }

  void Stabilisation::setDistanceToTarget(const cv::Rect& human) {
    setDistanceToTarget(human.height);
  }

  void Stabilisation::setDistanceToTarget(const std::vector<cv::Point2f>* points) {
    assert(points->size() >= MINIMAL_NUMBER_POINTS_FOLLOW);
    assert(points->size() >= 1);
    int minY = (*points)[0].y;
    int maxY = (*points)[0].y;
    for(unsigned int i = 0; i < points->size(); i++) {
      if((*points)[i].y < minY) {
	minY = (*points)[i].y;
      }
      if((*points)[i].y > maxY) {
	maxY = (*points)[i].y;
      }
    }
    assert(maxY - minY >= 0);
    setDistanceToTarget((maxY - minY) * 2);
  }

  double Stabilisation::estimateDistanceToTarget(const cv::Rect& human) {
    return estimateDistanceToTarget(human.height);
  }

  double Stabilisation::estimateDistanceToTarget(const int sizeY) {
    const int dist = sizeY - TARGET_SIZE;
    const double distance = (double)-dist;
    return distance * Kalman::CONSTANT_DISTANCE;
  }

  // radian
  double Stabilisation::estimateAngleToTarget(const int middleX) {
    const double middle = (double)middleX;
    const double FOV = 45.0d; // 45° on each side
    const double middleImage = (double)(Resource::getInstance().getWidth() >> 1);
    const double diff = middle - middleImage;
    const double angleDegree = -((diff / middleImage) * FOV);
    const double ret = ((angleDegree / 180) * Kalman::PI);
    if(middle > middleImage) { // right
      assert(ret <= 0.0d);
      assert(ret >= -FOV);
    } else { // left
      assert(ret >= 0.0d);
      assert(ret <= FOV);
    }
    return ret;
  }

  double Stabilisation::estimateAngleToTarget(const cv::Rect& human) {
    return estimateAngleToTarget(human.x + (human.width >> 1));
  }


  void Stabilisation::setDistanceToTarget(const int sizeY) {
    targetDistance_ = estimateDistanceToTarget(sizeY);
    //std::cout << "targetDistance_ " << targetDistance_ << std::endl;
  }

  /* Check if a point is inside a human
   *
   */
  bool Stabilisation::isInside(const peopledetect::HumanInfo& human,
			       const cv::Point2f& point) {
    const cv::Rect rectangle = human.getRectangle();
    const int width = rectangle.width;
    const int height = rectangle.height;
    const int x = rectangle.x;
    const int y = rectangle.y;
    return point.x >= x
      && point.y >= y
      && point.x < x + width
		   && point.y < y + height;
  }

  /* Count the number of points inside a person to check if
   * it's the right person
   */
  float Stabilisation::computeLikelyhoodHuman(const peopledetect::HumanInfo& human,
					      const std::vector<cv::Point2f>*
					      points) {
    assert(points->size() >= 1);
    const unsigned int size = points->size();
    unsigned int count = 0;
    for(unsigned int i = 0; i < size; i++) {
      if(isInside(human, (*points)[i])) {
	count++;
      }
    }
    const float fsize = (float)size;
    const float fcount = (float)count;
    const float ret = fcount / fsize;
    if(Debug::debug) {
      assert(ret >= 0.0f);
      assert(ret <= 1.0f);
    }
    return ret;
  }

  void Stabilisation::targetAnglePoint(const float x) {
    const int middleImage = Resource::getInstance().getWidth() >> 1;
    int diff = x - middleImage;
    /*
    float dTurn = ((float)diff) / ((float)middleImage);
    dTurn = 0.6 * dTurn;
    if(dTurn <= -1.0f) {
      dTurn = -1.0f;
    } else if(dTurn >= 1.0f) {
      dTurn = 1.0f;
    }
    ardrone::Drone::getInstance().turnRight(dTurn);
    */
    const int currentAngle = angleFloatToInt(ardrone::NavData::getInstance().getPsi());
    diff = diff / 10;
    if(diff < 0) {
      diff = diff + 360;
    }
    assert(diff >= 0);
    assert(diff < 360);
    targetAngle_ = angleAdd(currentAngle, diff);
  }

  void Stabilisation::targetAnglePoint(const cv::Rect& rectangle) {
    const int width = rectangle.width;
    //const int height = rectangle.height;
    const int x = rectangle.x;
    //const int y = rectangle.y;
    targetAnglePoint(x + (width >> 1) /*, y + (height >> 1)*/);
  }

  peopledetect::HumanInfo Stabilisation::getBestLikelyhood
  (std::vector<peopledetect::HumanInfo>* humanList) {

    assert(humanList->size() >= 1);
    peopledetect::HumanInfo bestHuman = (*humanList)[0];
    for(unsigned int i = 1; i < humanList->size(); i++) {
      if((*humanList)[i].getLikelihood() > bestHuman.getLikelihood()) {
	bestHuman = (*humanList)[i];
      }
    }
    return bestHuman;
  }

  cv::Rect* Stabilisation::getTargetCopy() {
    std::lock_guard<std::mutex> lock(mutexTarget_);
    if(humanTarget == nullptr) {
      return nullptr;
    } else {
      return new cv::Rect(*humanTarget);
    }
  }

  void Stabilisation::setTarget(const cv::Rect& human) {
    std::lock_guard<std::mutex> lock(mutexTarget_);
    if(humanTarget != nullptr) {
      delete(humanTarget);
    }
    humanTarget = new cv::Rect(human);
  }

}
