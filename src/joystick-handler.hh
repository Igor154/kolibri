#ifndef JOYSTICK_HANDLER_HH
# define JOYSTICK_HANDLER_HH

# include <thread>
# include <atomic>

# include "ui/joystick.hh"
# include "stabilisation/stabilisation.hh"

class JoystickHandler
{
public:
  JoystickHandler();
  ~JoystickHandler();

private:
  void run();

  Joystick _joystick;

  std::atomic<bool> _exit;
  std::thread _thread;
};

#endif
