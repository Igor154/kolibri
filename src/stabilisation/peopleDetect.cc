#include "peopleDetect.hh"


namespace peopleDetect {

  PeopleDetect::PeopleDetect():
    currentImage_(0),
    ref_(nullptr) {

  }

  PeopleDetect::~PeopleDetect() {
    if(ref_ != nullptr) {
      delete[](ref_);
    }
  }

  void PeopleDetect::findPeople(cv::Mat& src, std::vector<cv::Rect>& found)
  {
    cv::HOGDescriptor hog;
    hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
    hog.detectMultiScale(src, 
			 found, 
			 0, //hit threshold
			 cv::Size(8, 8), //cv::Size(8,16), Stride/le pas de décalage 
			 cv::Size(32, 32), //cv::Size(64,128), 
			 1.05,//scale 
			 2 //group threshold
			 );
  }

  void PeopleDetect::validateRect(cv::Rect& rect, const int width, const int height) {
    if(rect.x < 0) {
      rect.width = rect.width + rect.x;
      rect.x = 0;
    }
    if(rect.y < 0) {
      rect.height = rect.height + rect.y;
      rect.y = 0;
    }
    if(rect.x + rect.width >= width) {
      rect.width = width - rect.x;
    }
    if(rect.y + rect.height >= height) {
      rect.height = height - rect.y;
    }
  }

  float* PeopleDetect::getVector(cv::Mat& src) {
    int* vect = new int[256];
    for(unsigned int i = 0; i < 256; i++) {
      vect[i] = 0;
    }
    for(int x = 0; x < src.cols; x++) {
      for(int y = 0; y < src.rows; y++) {
	const int hValue = src.at<cv::Vec3b>(x,y)[0];
	assert(hValue >= 0 && hValue < 256);
	vect[hValue]++;
      }
    }
    const float total = (float)(src.cols * src.rows);
    float* ret = new float[256];
    for(unsigned int i = 0; i < 256; i++) {
      ret[i] = ((float)vect[i]) / total;
    }    
    
    delete[](vect);
    return ret;
  }

  float* PeopleDetect::copyVector(float* vector) {
    float* copy = new float[256];
    for(int i = 0; i < 256; i++) {
      copy[i] = vector[i];
    }
    return copy;
  }


  float PeopleDetect::getLikelyhood(float* vect1, float* vect2) {
    assert(vect1 != nullptr);
    assert(vect2 != nullptr);
    float totalDiff = 0.0f;
    for(unsigned int i = 0; i < 256; i++) {
      const float diff = vect1[i] - vect2[i];
      const float squaredDiff = diff * diff;
      assert(squaredDiff >= 0.0f);
      totalDiff = totalDiff + squaredDiff;
    }
    return totalDiff;
  }

  int PeopleDetect::getClosest(float** vects, const unsigned int size) {
    assert(size >= 1);
    assert(ref_ != nullptr);
    float bestScore = getLikelyhood(vects[0], ref_);
    int bestIndex = 0;
    for(unsigned int i = 1; i < size; i++) {
      const float score = getLikelyhood(vects[i], ref_);
      if(score > bestScore) {
	bestScore = score;
	bestIndex = i;
      }
    }
    //std::cout << "score : " << bestScore << std::endl;
    return bestIndex;
  }
  
  void PeopleDetect::detectPeople(cv::Mat& src) {
    // find people
    const int width = src.cols;
    const int height = src.rows;
    std::vector<cv::Rect> found;
    findPeople(src, found);

    // convert to hsv
    cv::Mat frame = src.clone();
    cv::cvtColor(frame, frame, CV_RGB2HSV);
    const unsigned int size = found.size();

    // get people
    std::vector<cv::Mat> people;
    for(unsigned int i = 0; i < size; i++) {
      cv::Rect& currentRect = found[i];
      validateRect(currentRect, width, height);
      //std::cout << currentImage_ << std::endl;
      /*
      std::cout << currentRect.x
		<< " " << currentRect.y
		<< " " << currentRect.x + currentRect.width
		<< " " << currentRect.y + currentRect.height << std::endl;
      */
      people.push_back(cv::Mat(frame, currentRect));
    }

    const float* background = getVector(frame);
    float** vects = new float*[size];
    for(unsigned int i = 0; i < size; i++) {
      float* currentVect = getVector(people[i]);
      // remove background
      for(unsigned int j = 0; j < 256; j++) {
        currentVect[j] = currentVect[j] - background[j];
      }
      vects[i] = currentVect;
    }

    // use datas
    int bestIndex = -1;
    if(ref_ != nullptr && size >= 1) {
      bestIndex = getClosest(vects, size);
    }


    // FOR TESTS ON DOUBLE RED
    if(currentImage_ == 13) {
      assert(size == 1);
      ref_ = copyVector(vects[0]);
      
    }

    // delete all
    for(unsigned int i = 0; i < size; i++) {
      delete[](vects[i]);
    }
    delete[](vects);
    delete[](background);

    // print
    const int size2 = found.size();
    for(int i = 0; i < size2; i++) {
      if(i == bestIndex) {
	rectangle(src, found[i], cv::Scalar(0, 0, 255));
      } else {
	rectangle(src, found[i], cv::Scalar(0, 255, 0));
      }
    }

    currentImage_++;

    cv::imshow("PeopleDetection", src);
    cv::waitKey(30);
  }





}
