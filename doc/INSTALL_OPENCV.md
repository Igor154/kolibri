# Install OpenCV with GPU capability (NVIDIA only)

## Prerequisites

* a working and usable NVIDA GPU under Linux (check your distrbution documentation to make sure that you have installed and configured the GPU properly)
* no existing OpenCV installation
* at least 6 GB of free disk space

## Steps

### Install CUDA

Install CUDA, e.g. from your package manager.

### Check the GPU capability

You need to check the capability of the GPU:

1. Locate and `cd` to the CUDA `samples` directory (usually in `/opt/cuda/samples/` or `~/NVIDIA_CUDA-X.Y_Samples/`)
2. `cd 1_Utilities/deviceQuery`
3. `sudo make && ./deviceQuery`
4. In the output, check and **remember** the value of the field *CUDA Capability Major/Minor version number*

### Download OpenCV

```
git clone git@github.com:Itseez/opencv.git
cd opencv
git checkout 2.4.11
```

### Configure OpenCV

```
mkdir release
cd release
cmake -D CMAKE_BUILD_TYPE=RELEASE -D ENABLE_FAST_MATH=1 -D WITH_CUBLAS=1 -D WITH_CUDA=1 -D CUDA_TOOLKIT_ROOT_DIR=/opt/cuda -D CUDA_GENERATION="" -D CUDA_ARCH_BIN=5.0 -D CUDA_ARCH_PTX=5.0 ..
```

**Important:** in the last command, change the following options according to your system:

* `CUDA_TOOLKIT_ROOT_DIR`: the root of the CUDA installation
* `CUDA_ARCH_BIN` and `CUDA_ARCH_PTX`: the *CUDA Capability Major/Minor version number*

Check in the output that CUDA is actually used, for example:

```
NVIDIA CUDA
--    Use CUFFT:         YES
--    Use CUBLAS:        YES
--    USE NVCUVID:       NO
--    NVIDIA GPU arch:   50
--    NVIDIA PTX archs:  50
--    Use fast math:     NO
```

where `NVIDIA GPU arch` and `NVIDIA PTX archs` refer to the *CUDA Capability Major/Minor version number*.

### Compile and install OpenCV

```
make -j4
sudo make install
```

**Note :** make sure to later enable the GPU within the program. See `GUI.md`.
