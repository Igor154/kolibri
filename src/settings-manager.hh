#pragma once

class SettingsManager
{
public:
  static SettingsManager& getInstance()
  {
    static SettingsManager manager;
    return manager;
  }

  void toggleGPU()
  {
    _gpuEnabled = !_gpuEnabled;
  }

  bool isGPUEnabled() const
  {
    return _gpuEnabled;
  }

private:
  static const bool gpuEnabledDefault = false;

  SettingsManager()
    : _gpuEnabled(gpuEnabledDefault)
  {
  }

  bool _gpuEnabled;
};
