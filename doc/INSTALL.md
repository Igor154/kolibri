# Compile the project

## Requirements

The following software and libraries are required:

* CMake
* Qt 5
* OpenCV 2.4.11

See `INSTALL_OPENCV.md` to install OpenCV with GPU capability.

## Build the source

Go in the project root directory and type:

```
mkdir build
cd build
cmake ..
make
```

**Note :** an Internet connection is necessary to build the source as the drone API will be fetched from `git://github.com/AutonomyLab/ardronelib.git`.
