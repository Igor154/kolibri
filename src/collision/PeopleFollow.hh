#ifndef PEOPLEFOLLOW_HH
# define PEOPLEFOLLOW_HH

# include <opencv2/core/core.hpp>
# include <opencv2/highgui/highgui.hpp>
# include <opencv2/video/tracking.hpp>
# include <opencv2/imgproc/imgproc.hpp>
# include <thread>
# include <atomic>
# include <mutex>

# include "../Resource.hh"

namespace collision {

  const int MINIMAL_FEATURES_ON_PERSON = 200;
  const int FEATURES_ON_PERSON = 100;
  
  class PeopleFollow {
  public:
    
    // lock
    void setNewTarget(const cv::Rect& sourceRectangle);
    // lock
    std::vector<cv::Point2f>* getCopyPointsToTrack();
    // lock
    void updatePointsToTrack(const cv::Mat& prevGrayImage,
			     const cv::Mat& grayImage);
    
    
    
    
    
  private:

    cv::Rect resizeRectangle(const cv::Rect& rectangle) const;
    bool isInsideRect(const cv::Point2f& point, const cv::Rect& rectangle) const;
    unsigned int countPointsInRectangle(const std::vector<cv::Point2f>& list,
					const cv::Rect& rectangle) const;
    bool needToChangeTarget(const cv::Rect& sourceRectangle) const;
        
    std::vector<cv::Point2f> pointsToTrack_;
    std::mutex mutexToTrack_;
    
    std::vector<cv::Point2f> newPositions_;
    
    
};
}

#endif /* PEOPLEFOLLOW_HH */
