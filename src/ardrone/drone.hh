#ifndef ARDRONE_DRONE_HH
# define ARDRONE_DRONE_HH

# include <thread>
# include <atomic>

namespace ardrone
{
  class Drone
  {
  public:
    static constexpr const float defaultSpeed = 1.0f;
    static const unsigned int commandsSendingDelayInMs = 30;
    static const int defaultAltitude = 2000;
    static const int minAltitude = 200;
    static const int maxAltitude = 3000;
    static const int altitudeStep = 100;
    static const int altitudeErrorMargin = 30;
    static const int altitudeNormalization = 1000;
    static constexpr const char* const altitudeUnit = "mm";

    static Drone& getInstance();

    int getVersion() const;
    bool isVersion2() const;
    bool isVersion1() const;

    void init();

    void takeOff();
    void land();

    // Move methods:
    // - the given orders are sent every 30 ms to the drone until ordered otherwise.
    // - speed argument is comprised between 0.0f and 1.0f.

    void moveForward(float speed = defaultSpeed);
    void moveBackward(float speed = defaultSpeed);
    void stopMovingHorizontally(); // = moveForward(0.0) = moveBackward(0.0)

    void moveLeft(float speed = defaultSpeed);
    void moveRight(float speed = defaultSpeed);
    void stopMovingLaterally(); // = moveLeft(0.0) = moveRight(0.0)

    void turnLeft(float speed = defaultSpeed);
    void turnRight(float speed = defaultSpeed);
    void stopTurning(); // = turnLeft(0.0) = turnRight(0.0)

    void stopMoving(); // = stopTurning() + stopMovingLaterally() + stopMovingHorizontally()

    void setRelativeAltitude(int height); // in millimeters compared to the current altitude
    void setAbsoluteAltitude(int height); // in millimeters compared to the ground

    // Change altitude by a predefined step
    void increaseAltitude();
    void decreaseAltitude();

  private:
    Drone();
    ~Drone();

    void run();

    std::thread controller_;
    std::atomic<float> pitch_;
    std::atomic<float> roll_;
    std::atomic<float> yaw_;
    std::atomic<float> height_;
    std::atomic<int> targetHeight_;

    bool emergencyState_;

    std::atomic<bool> exit_;
  };
}

#endif
