#include "video.hh"
#include "navdata.hh"

#ifdef __cplusplus
extern "C"
{
#endif

#include "post_stage.hh"

const vp_api_stage_funcs_t post_stage_funcs = {
  NULL,
  (vp_api_stage_open_t) post_stage_open,
  (vp_api_stage_transform_t) post_stage_transform,
  (vp_api_stage_close_t) post_stage_close
};

// Picture size getter from input buffer size
// This function only works for RGB565 buffers (i.e. 2 bytes per pixel)
static void getPicSizeFromBufferSize (uint32_t bufSize, uint32_t *width, uint32_t *height)
{
  if (NULL == width || NULL == height)
  {
    return;
  }

  switch (bufSize)
  {
  case 50688: //QCIF > 176*144 *2bpp
    *width = 176;
    *height = 144;
    break;
  case 153600: //QVGA > 320*240 *2bpp
    *width = 320;
    *height = 240;
    break;
  case 460800: //360p > 640*360 *2bpp
    *width = 640;
    *height = 360;
    break;
  case 1843200: //720p > 1280*720 *2bpp
    *width = 1280;
    *height = 720;
    break;
  default:
    *width = 0;
    *height = 0;
    break;
  }
}

// Get actual frame size (without padding)
void getActualFrameSize (post_stage_cfg_t *cfg, uint32_t *width, uint32_t *height)
{
  if (NULL == cfg || NULL == width || NULL == height)
  {
    return;
  }

  *width = cfg->decoder_info->width;
  *height = cfg->decoder_info->height;
}

C_RESULT post_stage_open (post_stage_cfg_t *cfg)
{
  return C_OK;
}

C_RESULT post_stage_transform (post_stage_cfg_t *cfg, vp_api_io_data_t *in, vp_api_io_data_t *out)
{
  // Copy in to out
  out->size = in->size;
  out->status = in->status;
  out->buffers = in->buffers;
  out->indexBuffer = in->indexBuffer;

  uint32_t width = 0;
  uint32_t height = 0;
  getActualFrameSize (cfg, &width, &height);

  ardrone::Video::getInstance().updateFrame(in->buffers[0],
                                            width,
                                            height,
                                            ardrone::NavData::getInstance().getPhi(),
                                            ardrone::NavData::getInstance().getTheta());

  return C_OK;
}

C_RESULT post_stage_close (post_stage_cfg_t *cfg)
{
  return C_OK;
}

#ifdef __cplusplus
}
#endif
