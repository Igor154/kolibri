#include "HumanRTH.hh"
#include "../Debug.hh"

namespace peopledetect {

  HumanRTH::HumanRTH(const cv::Rect rectangle, float likelihood):
    rectangle_(rectangle),
    likelihood_(likelihood) {
    if(Debug::debug) {
      assert(likelihood >= 0.0d);
      assert(likelihood <= 1.0d);
    }
  }

  HumanRTH::~HumanRTH() {

  }

  const cv::Rect& HumanRTH::getRectangle() const {
    return rectangle_;
  }

  float HumanRTH::getLikelihood() const {
    return likelihood_;
  }

}
