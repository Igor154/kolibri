/**
 * Post decoding stage that dump the raw decoded video
 */

#ifndef _POST_STAGE_H_
#define _POST_STAGE_H_ (1)

typedef struct _video_decoder_config_t video_decoder_config_t;

#include <ardrone_tool/Video/video_stage.h>
#include <inttypes.h>

typedef struct _post_stage_cfg_ {
  // PARAM
  float bpp;
  vp_api_picture_t *decoder_info;
} post_stage_cfg_t;

C_RESULT post_stage_open (post_stage_cfg_t *cfg);
C_RESULT post_stage_transform (post_stage_cfg_t *cfg, vp_api_io_data_t *in, vp_api_io_data_t *out);
C_RESULT post_stage_close (post_stage_cfg_t *cfg);

extern const vp_api_stage_funcs_t post_stage_funcs;

#endif // _POST_STAGE_H_
