#pragma once

#include <iostream>
#include "helper.hh"
#include "HumanInfo.hh"

namespace peopledetect
{
  class Human : public HumanInfo
  {
  public:
    Human(cv::Mat& src, cv::Rect& selection);
    ~Human();
    // PROPERTIES
    //std::string name = "No Name";
    int notActiveCount = 0;
    static int nb_human_created;
    
    // METHODS
    void compute_mean_hue_buckets();
    void likeLihoodUpdate(Human& target);
    // static
    static void sanity_check33(peopledetect::Human* currentTarget, std::vector<peopledetect::Human>* humanList)
    {

     if (currentTarget == nullptr )
       return;

     if(humanList == nullptr)
       return;

     if(humanList->empty())
       return;

     /*
     for (unsigned int i = 0; i < humanList->size(); ++i)
    {
      //rectangle a l'exterieur de la target
      if (abs(currentTarget->getRect().x - (*humanList)[i].getRect().x) > currentTarget->getRect().width)
      {
 	humanList->erase(humanList->begin() + i--);
	continue;
      }
      if (abs(currentTarget->getRect().y - (*humanList)[i].getRect().y) > currentTarget->getRect().height)
      {
	humanList->erase(humanList->begin() + i--);
	continue;
      }
    }
     */

    }

    // GETTERS
    std::vector<unsigned int>& getHueBuckets();
    const cv::Rect& getRect() const;
    const cv::Rect& getRectangle() const;
    float getLikelihood() const;
    bool getActive() const;

    // SETTERS
    void setRect(cv::Rect rect);
    void setActive(bool val);
    void setLikelihood(float f);

  private:
    // PROPERTIES
    // static
    //   Hue buckets
    static const unsigned int NB_HUE_BUCKETS;
    static const unsigned int NB_HUE_PER_BUCKET;
    // nonstatic
    std::vector<unsigned int> hue_buckets_percent;
    float likelihood = 0.0f;
    cv::Rect rect;
    unsigned int totalPixels = 0;
  };
}
