#ifndef COLLISION_HH
# define COLLISION_HH

# include <opencv2/core/core.hpp>
# include <opencv2/highgui/highgui.hpp>
# include <opencv2/video/tracking.hpp>
# include <opencv2/imgproc/imgproc.hpp>
# include <thread>
# include <atomic>
# include <mutex>

# include "PeopleFollow.hh"
# include "../Resource.hh"
# include "../peopledetect/human.hh"

namespace collision {

class Collision {
public:

  const double THRESHOLD_DANGER = 3 * 1000000.0; // before log
  const unsigned int THRESHOLD_LOST = 60;

  const float MINIMAL_DISTANCE_BETWEEN_POINTS = 8.0f;
  const unsigned int MAX_COUNT_FEATURES = 200;
  
  Collision(unsigned int targetFps);
  ~Collision();

  void update(cv::Mat mat);

  void init();

  void copyDebugFrameTo(cv::Mat& mat);
  std::chrono::milliseconds getLastExecutionTime() const;

  void analyseImage(cv::Mat& currentFrame);
  void translateImg(cv::Mat &img, int offsetx, int offsety);

  bool mustStop();
  bool isLost(); // not enough points to determine danger
  void resetDanger();

  float getDangerLeft();
  float getDangerRight();

  void setNewTarget(const peopledetect::HumanInfo* human);

std::vector<cv::Point2f>* getCopyPointsToTrack();

private:

  void setDebugFrame(const cv::Mat& mat);

  bool isLeft(cv::Point2f& point, int width_middle);

  std::chrono::system_clock::time_point getTimeNow();

  double getTimeCoef(double timeFloat);


  cv::Scalar getRandomColor();

  void sortListWidth(std::vector<cv::Point2f>& listAllCurrent,
		     std::vector<cv::Point2f>& listAllPrev);
  void sortListHeight(std::vector<cv::Point2f>& listAllCurrent,
		      std::vector<cv::Point2f>& listAllPrev);

  float getBestDangerWidth(std::vector<cv::Point2f>& listAllCurrent,
			   std::vector<cv::Point2f>& listAllPrev,
			   cv::Mat& image);
  float getBestDangerHeight(std::vector<cv::Point2f>& listAllCurrent,
			    std::vector<cv::Point2f>& listAllPrev,
			    cv::Mat& image);

  bool removeUselessPoints(std::vector<cv::Point2f>& listAllCurrent,
			   std::vector<cv::Point2f>& listAllPrev);

  void updatePointsToTrack(const cv::Mat& prevGrayImage,
			   const cv::Mat& grayImage);

  bool firstCall_;

  std::vector<cv::Point2f> points_[2];

  cv::Mat prevGray_;
  std::chrono::system_clock::time_point prevTime_;

  double dangerCounter_;
  bool lost_;

  float dangerLeft_;
  float dangerRight_;

  cv::Mat debugFrame_;
  std::mutex mutexDebugFrame_;

  unsigned int targetFps_;
  std::atomic<std::chrono::milliseconds> lastExecutionTime_;

  PeopleFollow peopleFollow_;

};
}

#endif /* COLLISION_HH */
