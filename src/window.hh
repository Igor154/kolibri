#ifndef WINDOW_HH
# define WINDOW_HH

# include <opencv2/core/core.hpp>
# include <QtWidgets>
# include <thread>
# include <atomic>

# include "ui/video-label.hh"
# include "ardrone/thread.hh"
# include "collision/collision.hh"
# include "stabilisation/stabilisation.hh"
# include "peopledetect/peopledetectmanager.hh"
# include "joystick-handler.hh"

class Window: public QMainWindow
{
  Q_OBJECT

public:
  Window(int argc, char* argv[]);

private slots:
  void about();
  void exit();

  void droneConnect();
  void droneDisconnect();
  void checkConnection();

  void land();
  void takeOff();

  void start();
  void stop();
  void cancelTarget();

  void updateFrame();
  void updateNavData();
  void updateExecutionTimes();

  void startRecording();
  void stopRecording();

  void updateAltitude(int altitude);

  void handleFrameClick(int x, int y);

  void showDefaultVideo();
  void showCollisionDebug();
  void togglePeopleHighlighting();

  void toggleGPU();

protected:
  void keyPressEvent(QKeyEvent *event);
  void keyReleaseEvent(QKeyEvent *event);

private:
  enum videoView
  {
    DEFAULT,
    COLLISION
  };

  void executionLoop();

  void closeEvent(QCloseEvent *bar);

  void highlightPeople(cv::Mat& mat);

  void createActions();
  void createMenus();
  void createStatusBar();
  void createToolBars();
  void createControls();
  void createTextField(const std::string& name,
                       const std::string& defaultValue,
                       QLabel*& label,
                       QBoxLayout* parent);

  void waitForConnection();

  void startDroneUpdate();
  void stopDroneUpdate();

  void startExecutionTimesUpdate();
  void stopExecutionTimesUpdate();

  QTimer *_timer;
  QTimer *_connectionTimer;
  QTimer *_executionTimesTimer;

  ui::VideoLabel *_frameLabel;
  QSpinBox *_altitudeControl;

  QMenu *_fileMenu;
  QMenu *_viewMenu;
  QMenu *_settingsMenu;
  QMenu *_helpMenu;

  QToolBar *_droneToolBar;
  QToolBar *_controlToolBar;
  QToolBar *_recordToolBar;

  QAction *_exitAct;
  QAction *_aboutAct;
  QAction *_connectAct;
  QAction *_takeOffAct;
  QAction *_landAct;
  QAction *_startAct;
  QAction *_stopAct;
  QAction *_cancelTargetAct;
  QAction *_recordAct;
  QAction *_stopRecordAct;
  QAction *_gpuToggleAct;

  QActionGroup *_videoView;
  QAction *_defaultVideoAct;
  QAction *_collisionDebugAct;
  QAction *_highlightPeopleAct;

  QLabel *_batteryLabel;
  QLabel *_altitudeLabel;
  QLabel *_thetaLabel;
  QLabel *_phiLabel;
  QLabel *_psiLabel;
  QLabel *_vxLabel;
  QLabel *_vyLabel;
  QLabel *_vzLabel;

  QLabel *_collisionTime;
  QLabel *_peopleDetectTime;
  QLabel *_autoFlightState;

  QVBoxLayout *_mainLayout;
  QHBoxLayout *_videoLayout;
  QHBoxLayout *_bottomLayout;

  ardrone::Thread* _droneThread;
  stabilisation::Stabilisation* _stabilisationThread;
  std::thread* _executionThread;
  std::atomic<bool> _exitExecutionThread;

  collision::Collision* _collision;
  peopledetect::PeopleDetectHelper* _people;

  JoystickHandler* _joystickHandler;

  int _argc;
  char** _argv;
  enum videoView _currentView;
  bool _highlightPeople;
};

#endif
