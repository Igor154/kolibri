#pragma once

#include "HumanInfo.hh"

namespace peopledetect {

  class HumanRTH : public HumanInfo {
  public:

    HumanRTH(const cv::Rect rectangle, float likelihood_);
    ~HumanRTH();

    virtual const cv::Rect& getRectangle() const;
    virtual float getLikelihood() const;

  private:
    cv::Rect rectangle_;
    float likelihood_;
  };

}
