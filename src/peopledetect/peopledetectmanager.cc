#include "peopledetectmanager.hh"

#include "../tools/scoped-timer.hh"
#include "../Debug.hh"

namespace peopledetect
{
  PeopleDetectManager::PeopleDetectManager(unsigned int targetFps_)
    :pd(),
     humanList(new std::vector<Human>()),
     newList(nullptr),
     oldList(nullptr),
     targetFps(targetFps_),
     lastExecutionTime_(std::chrono::milliseconds(0))
  {
  }
  
  PeopleDetectManager::~PeopleDetectManager()
  {
  }

  void PeopleDetectManager::update(cv::Mat mat)
  {
    tools::ScopedTimer<std::chrono::milliseconds> timer(std::chrono::milliseconds(1000 / targetFps));

    if (!mat.empty())
    {
      newList = pd.detectPeople(mat);
      mutex.lock();
      oldList = humanList;
      humanList = newList;
      mutex.unlock();
      delete(oldList);
    }

    lastExecutionTime_ = timer.getElapsedTime();
  }

  std::chrono::milliseconds PeopleDetectManager::getLastExecutionTime() const
  {
    return lastExecutionTime_;
  }

  std::vector<HumanInfo*>* PeopleDetectManager::getCopyHumans()
  {
    std::vector<HumanInfo*>* newlist = new std::vector<HumanInfo*>();
    mutex.lock();
    
    for (unsigned int i = 0; i < this->humanList->size(); ++i)
    {
      Human h = (*this->humanList)[i];
      newlist->push_back(new Human(h));
    }

    mutex.unlock();

    return newlist; 
  }

  /*
  Human* PeopleDetectManager::getTargetCopy()
  {
    return this->pd.getTargetCopy();
  }
  */

  HumanInfo* PeopleDetectManager::setNewTarget(int X, int Y)
  {
    return pd.setNewTarget(X,Y);
  }

  void PeopleDetectManager::recalibrateTarget(const HumanInfo& h)
  {
    pd.recalibrateTarget(h);
  }

  void PeopleDetectManager::removeTarget()
  {
    pd.removeTarget();
  }

  // threshold to determine if it's a the same human or not
  float PeopleDetectManager::getThreshold() {
    const float value = 0.3;
    if(Debug::debug) {
      assert(value >= 0.0f);
      assert(value <= 1.0f);
    }
    return value;
  }
}
