#pragma once

#include <vector>
#include <deque>
#include <utility>
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>
#include <cassert>
#include <cmath>

#include "../ardrone/navdata.hh"
#include "../ardrone/drone.hh"

namespace stabilisation {

  typedef std::chrono::high_resolution_clock::time_point time_point;

  class TargetPosition {
  public:
    TargetPosition(const time_point timePoint,
                   const bool isPrediction,
		   const double posX,
		   const double posY);
    time_point getTimePoint() const;
    bool getIsPrediction() const;
    double getPosX() const;
    double getPosY() const;
  private:
    const time_point timePoint_;
    const bool isPrediction_;
    double posX_;
    double posY_;
  };


  class Kalman {
    typedef std::pair<double, double> Pair;
  public:
    static constexpr const double PI = 3.14159265359d;
    static constexpr const double HALF_PI = 3.14159265359d / 2.0d;
    // temps durant lequel la position de la cible est conserve
    static constexpr const double MAX_TIME_POSITION = 15.0d; // in seconds
    static constexpr const double CONSTANT_DISTANCE = 800.0d;
    static constexpr const double BASE_DISTANCE = 200.0d * CONSTANT_DISTANCE;
    static constexpr const double MIN_DISTANCE = 20.d * CONSTANT_DISTANCE;


    Kalman();
    ~Kalman();


    void update(const ardrone::NavData& data);
    
    void recalibrate(const double distanceToTarget,
		     const double angleToTarget,
		     const ardrone::NavData& data);
    void notCalibrate();
    // in radian
    double angleToTarget();
    // return distance to target
    double isTarget(const double distanceToTarget,
		    const double angleToTarget,
		    const ardrone::NavData& data);
    void reset();


  private:
    void updateDronePosition(const ardrone::NavData& data);
    void removeOldTargetPosition();
    void resetExpectedPosition();
    void updateExpectedPosition();
    void updateExpectedSpeed();

    double getDistance(const double x1,
		       const double y1,
		       const double x2,
		       const double y2);
    
    time_point timeNow() const;
    // in seconds
    double elapsedTime(const time_point& start, const time_point& end) const;
    double degreeToRadian(const double angle);    


    // historic of positions
    // oldest -> last
    // newest -> front
    std::deque<TargetPosition> targetPositions_;

    Pair dronePosition_;

    // where the target should be
    Pair expectedPosition_;
    Pair expectedSpeed_;

  };
}
