#include <iostream>

#include "PeopleFollow.hh"
#include "../Debug.hh"

namespace collision {

  //const cv::TermCriteria termcrit(cv::TermCriteria::COUNT|
  //				  cv::TermCriteria::EPS,20,0.03);
  const cv::TermCriteria termcrit(cv::TermCriteria::COUNT|
				  cv::TermCriteria::EPS, 20, 0.03);
  const cv::Size subPixWinSize(10,10), winSize(31, 31);

  // lock
  void PeopleFollow::setNewTarget(const cv::Rect& sourceRectangle) {
    if(sourceRectangle.width <= 10 || sourceRectangle.height <= 10) {
      return; // too small -> dangerous for next step
    }
    if(!needToChangeTarget(sourceRectangle)) {
      return;
    }
    cv::Rect rectangle = resizeRectangle(sourceRectangle);
    // extract sub matrix
    cv::Mat grayPerson;
    {
      const cv::Mat originalMat = Resource::getInstance().getCopyMat(true);
      const cv::Mat tmp = originalMat(rectangle);
      cv::Mat person;
      tmp.copyTo(person);
      cv::cvtColor(person, grayPerson, cv::COLOR_BGR2GRAY);
    }

    std::vector<cv::Point2f> pointsToAdd;
    {
      cv::goodFeaturesToTrack(grayPerson,
			      pointsToAdd,
			      FEATURES_ON_PERSON,
			      0.01,
			      5, /* 10 */
			      cv::Mat(),
			      3,
			      0,
			      0.04);
      cv::cornerSubPix(grayPerson, pointsToAdd, subPixWinSize,
		       cv::Size(-1,-1), termcrit);
    }
    
    {
      const std::lock_guard<std::mutex> lock(mutexToTrack_);
      pointsToTrack_.clear();
      // add points in list
      const float baseX = rectangle.x;
      const float baseY = rectangle.y;
      for(unsigned int i = 0; i < pointsToAdd.size(); i++) {
	cv::Point2f currentPoint = pointsToAdd[i];
	currentPoint.x = currentPoint.x + baseX;
	currentPoint.y = currentPoint.y + baseY;
	pointsToTrack_.push_back(currentPoint);
      }
    }
    newPositions_.clear();
  }

  // lock
  std::vector<cv::Point2f>* PeopleFollow::getCopyPointsToTrack() {
    const std::lock_guard<std::mutex> lock(mutexToTrack_);
    std::vector<cv::Point2f>* list = new std::vector<cv::Point2f>();
    const unsigned int size = pointsToTrack_.size();
    for(unsigned int i = 0; i < size; i++) {
      list->push_back(pointsToTrack_[i]);
    }
    return list;
  }

  // lock
  void PeopleFollow::updatePointsToTrack(const cv::Mat& prevGrayImage,
					 const cv::Mat& grayImage) {
    const std::lock_guard<std::mutex> lock(mutexToTrack_);
    if(!pointsToTrack_.size()) {
      return; // do nothing
    }

    std::vector<uchar> status;
    std::vector<float> err;
    cv::calcOpticalFlowPyrLK(prevGrayImage,
			     grayImage,
			     pointsToTrack_,
			     newPositions_,
			     status,
			     err,
			     winSize,
			     3,
			     termcrit,
			     0,
			     0.001f);
    
    {
      const unsigned int size = newPositions_.size();
      unsigned int k = 0;
      for(unsigned int i = 0; i < size; i++) {
	if(status[i]) {
	  newPositions_[k] = newPositions_[i];
	  k++;
	}
      }
      newPositions_.resize(k);
    }

    std::swap(newPositions_, pointsToTrack_);
  }





  // private

  cv::Rect PeopleFollow::resizeRectangle(const cv::Rect& rectangle) const {
    int x = rectangle.x;
    int y = rectangle.y;
    int width = rectangle.width;
    int height = rectangle.height;

    x = x + (width >> 2);
    y = y + (height >> 2);
    width = width >> 1;
    height = height >> 1;
    
    return cv::Rect(x, y, width, height);
  }

  bool PeopleFollow::isInsideRect(const cv::Point2f& point, const cv::Rect& rectangle) const {
    return point.x >= rectangle.x
      && point.y >= rectangle.y
      && point.x < rectangle.x + rectangle.width
		   && point.y < rectangle.y + rectangle.height;
  }


  unsigned int PeopleFollow::countPointsInRectangle(const std::vector<cv::Point2f>& list,
						    const cv::Rect& rectangle) const {
    unsigned int ret = 0;
    const unsigned int size = list.size();
    for(unsigned int i = 0; i < size; i++) {
      if(isInsideRect(list[i], rectangle)) {
	ret++;
      }
    }
    return ret;
  }

  bool PeopleFollow::needToChangeTarget(const cv::Rect& sourceRectangle) const {
    const unsigned int count = countPointsInRectangle(pointsToTrack_, sourceRectangle);
    const unsigned int total = pointsToTrack_.size();
    return count * 3 <= total * 4;
  }
  
  
}
