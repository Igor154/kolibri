#include <iostream>
#include "joystick-handler.hh"
#include "ardrone/drone.hh"

JoystickHandler::JoystickHandler()
  : _joystick(0)
  , _thread(&JoystickHandler::run, this)
{
  if (!_joystick.isFound())
    _exit = true;
}

JoystickHandler::~JoystickHandler()
{
  _exit = true;
  _thread.join();
}

void JoystickHandler::run()
{
  while (!_exit)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    JoystickEvent event;

    if (_joystick.sample(&event))
    {
      if (event.isButton() && event.value != 0)
      {
        switch (event.number)
        {
        case 1: // X
          ardrone::Drone::getInstance().takeOff();
          break;
        case 2: // O
          ardrone::Drone::getInstance().land();
          break;
        case 6: // L2
          ardrone::Drone::getInstance().decreaseAltitude();
          break;
        case 7: // R2
          ardrone::Drone::getInstance().increaseAltitude();
          break;
        case 9: // Start
          break;
        default:
          break;
        }
      }
      else if (event.isAxis())
      {
        double value = event.value / JoystickEvent::MAX_AXES_VALUE;

        switch (event.number)
        {
        case 0: // Left joystick, left-right axis
          ardrone::Drone::getInstance().moveRight(value);
          break;
        case 1: // Left joystick, up-down
          ardrone::Drone::getInstance().moveBackward(value);
          break;
        case 2: // Right joystick, left-right axis
          ardrone::Drone::getInstance().turnRight(value);
          break;
        case 3: // Right joystick, up-down axis
          break;
        default:
          break;
        }
      }
    }
  }
}
