#pragma once

#include "../Resource.hh"
#include "human.hh"
#include "peopledetect2.hh"
#include <mutex>
#include <thread>
#include <atomic>

#include "PeopleDetectHelper.hh"

namespace peopledetect
{
  class PeopleDetectManager : public PeopleDetectHelper
  {
  public:
    PeopleDetectManager(unsigned int targetFps_);
    ~PeopleDetectManager();

    void update(cv::Mat mat);

    std::chrono::milliseconds getLastExecutionTime() const;

    std::vector<HumanInfo*>* getCopyHumans();
    //Human* getTargetCopy(); // useless (check by Romaric)
    HumanInfo* setNewTarget(int X, int Y);
    void recalibrateTarget(const HumanInfo& h);
    void removeTarget();  
    float getThreshold();

  private:
    PeopleDetect2 pd;
    std::vector<Human>* humanList;
    std::vector<Human>* newList;
    std::vector<Human>* oldList;
    std::mutex mutex;
    unsigned int targetFps;
    std::atomic<std::chrono::milliseconds> lastExecutionTime_;
  };
}
