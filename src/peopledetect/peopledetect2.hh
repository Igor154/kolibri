#pragma once

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/gpu/gpu.hpp"

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <mutex>
#include <limits>

#include "human.hh"

namespace peopledetect
{
class PeopleDetect2
{
public:
  //constructor
  PeopleDetect2();
  ~PeopleDetect2();

  // PROPERTIES
  std::mutex mutex;
  // METHODS
  std::vector<Human>* detectPeople(cv::Mat& src);
  HumanInfo* setNewTarget(int X, int Y);
  void recalibrateTarget(const HumanInfo& h);
  void removeTarget();
  // GETTER
  Human* getTargetCopy();
private:
  // PROPERTIES
  Human* currentTarget = NULL;
  std::vector<Human> latestHumanList;
  unsigned int nbUpWithoutAnyTarget;
  unsigned int latestHumanList_updates;
  
  // METHODS
  void cpu_compute(cv::Mat& src, std::vector<cv::Rect>& found);
  void gpu_compute(cv::Mat& src, std::vector<cv::Rect>& found);
  
  void makeHumans(cv::Mat& srcImg,
		  std::vector<cv::Rect>& rects,
		  std::vector<Human>& humans);
  void updateCurrentTarget(std::vector<Human>& humans);
  void setNewCurrentTarget(Human human);
  void drawDebug(std::vector<Human>* returnVector, cv::Mat imgCopy, double time);
  
};
}
