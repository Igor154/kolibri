#ifndef ARDRONE_EXEC_HH
# define ARDRONE_EXEC_HH

# include <stdio.h>
# include <VP_Os/vp_os_types.h>

typedef struct _video_decoder_config_t video_decoder_config_t;

void ardrone_signal_exit();
void ardrone_signal_start();

bool ardrone_is_connected();

int ardrone_main(int argc, char* argv[]);

#endif
