#pragma once

#include "PeopleDetectHelper.hh"
#include "HumanRTH.hh"

# include <atomic>
# include <chrono>
# include <iostream>
# include <thread>
# include <mutex>

namespace peopledetect {

  class PeopleDetectRTH : public PeopleDetectHelper {
  public:

    PeopleDetectRTH(unsigned int targetFps);
    ~PeopleDetectRTH();

    void update(cv::Mat mat);
    std::vector<HumanInfo*>* getCopyHumans(); // must be free by caller
    void recalibrateTarget(const HumanInfo& target);
    std::chrono::milliseconds getLastExecutionTime() const;
    void removeTarget();
    HumanInfo* setNewTarget(const int x, const int y);
    float getThreshold();


  private:

    bool isValidRect(const cv::Rect& rect);
    void resizeRect(cv::Rect& rect, const int width, const int height);

    bool isInside(const cv::Rect& rect, const int x, const int y);
    // get distance between middle of rect and the other point
    double getDistance(const cv::Rect& rect, const double x2, const double y2);
    double getDistance(const double x1, const double y1, const double x2, const double y2);

    void updateCopy(cv::Mat copy);
    cv::Rect getCenter(const cv::Rect rect);

    float extractLikelihood(const cv::Mat src);

    void gpu_compute(cv::Mat& src, std::vector<cv::Rect>& found);
    void cpu_compute(cv::Mat& src, std::vector<cv::Rect>& found);
    void people_compute(cv::Mat& src, std::vector<cv::Rect>& found);

    double min(double nbr1, double nbr2) const;
    double circleDistance(double nbr1, double nbr2) const;
    double normalDistance(double nbr1, double nbr2) const;

    void initialiseTarget(cv::Mat& hsv);
    void updateTarget(cv::Mat& hsv);

    // variables

    const static int WIDTH = 64;
    const static int HEIGHT = 128;
    const static int SIZE = WIDTH * HEIGHT;
    //const static int MAX_VALUE = SIZE * ((255 / 2) + 1);
    const static int MAX_VALUE = SIZE * (255 * 3);

    const static constexpr double DEPRECIATION_COEF = 0.01d;
    const static constexpr double INVERSE_COEF = 1.0d - DEPRECIATION_COEF;

    //double stockH[SIZE];
    double stockR[SIZE];
    double stockG[SIZE];
    double stockB[SIZE];
    double stockVariance[SIZE];

    bool initialised;
    float threshold_;

    std::mutex mutex_;
    
    // result
    std::vector<HumanRTH> humanFound;

    std::atomic<std::chrono::milliseconds> lastExecutionTime_;
    unsigned int targetFps_;
  };

}
