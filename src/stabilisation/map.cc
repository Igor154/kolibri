#include <iostream>
#include <cassert>

#include "map.hh"

namespace stabilisation {

  /*
  Map::Map():
    updater_(&Map::update, this),
    keepUpdating_(true),
    posX_(0.0f),
    posY_(0.0f),
    mat_(MAP_WIDTH, MAP_HEIGHT, CV_8UC3),
    prevPoint_(MAP_WIDTH / 2, MAP_HEIGHT / 2) {
    rectangle(mat_,
	      cv::Point2f(0, 0),
	      cv::Point2f(MAP_WIDTH, MAP_HEIGHT),
	      cv::Scalar(0, 0, 0), CV_FILLED);
  }

  Map::~Map() {
    keepUpdating_ = false;
    updater_.detach();
  }

  float Map::addAngle(float angle1, float angle2) {
    float total = angle1 + angle2;
    while(total > 180000.0f) {
      total = total - 360000.0f;
    }
    while(total < -180000.0f) {
      total = total + 360000.0f;
    }
    return total;
  }

  float Map::degreeToRadian(float angle) {
    const float value = (angle / 180000.0f) * M_PI;
    assert(value >= -M_PI && value <= M_PI);
    return value;
  }

  void Map::update() {
    const int waitTime = 10;
    const float inv = 1.0f / (float)waitTime;
    std::chrono::duration<int, std::milli> waitingTime(waitTime);
    while(keepUpdating_) {
      std::this_thread::sleep_for(waitingTime);

      const float speedX = helidata.vx;
      const float speedY = helidata.vy;
      const float angle = helidata.psi;
      assert(angle <= 180000.0f && angle >= -180000.0f);

      const float angleX = degreeToRadian(angle);
      const float angleY = degreeToRadian(addAngle(angleX, 90000.0f));

      const float moveX = ((speedX * cos(angleX)) - (speedY * cos(angleY))) * inv;
      const float moveY = ((speedX * sin(angleX)) - (speedY * sin(angleY))) * inv;

      posX_ = posX_ + moveX;
      posY_ = posY_ + moveY;

      const float DIV = 50.0f;
      const float mapX = posX_ / DIV;
      const float mapY = posY_ / DIV;
      cv::Point2f currentPoint(mapX + (MAP_WIDTH / 2), mapY + (MAP_HEIGHT / 2));

      std::cout << "*****" << std::endl;
      std::cout << "posX : " << posX_ << std::endl;
      std::cout << "posY : " << posY_ << std::endl;
      std::cout << "battery : " << helidata.battery << std::endl;

      line(mat_, currentPoint, prevPoint_, cv::Scalar(255, 255, 255));
      //cv::imshow("Map", mat_);
      //cv::waitKey(30);

      prevPoint_.x = currentPoint.x;
      prevPoint_.y = currentPoint.y;
    }
  }
*/


}
