#ifndef STABILISATION_HH
# define STABILISATION_HH

# include <atomic>
# include <chrono>
# include <iostream>
# include <thread>
# include <mutex>

# include "../ardrone/navdata.hh"
# include "../ardrone/drone.hh"
# include "../collision/collision.hh"
# include "../peopledetect/PeopleDetectHelper.hh"
# include "map.hh"
# include "Kalman.hh"


namespace stabilisation {

  const unsigned int MINIMAL_NUMBER_POINTS_FOLLOW = 6;
  const float MINIMAL_THRESHOLD = 0.6f;
  const int WAITING_TIME = 50;
  const int TARGET_SIZE = 180; // 200

class Stabilisation {

public:
  Stabilisation(collision::Collision& collision,
		peopledetect::PeopleDetectHelper& people);
  ~Stabilisation();

  bool getManualFlight() const;
  void setManualFlight(bool newValue);
  cv::Rect* getTargetCopy();

  static double estimateDistanceToTarget(const int sizeY);
  static double estimateDistanceToTarget(const cv::Rect& human);
  // positive on the LEFT, negative on the RIGHT
  static double estimateAngleToTarget(const int middleX);
  static double estimateAngleToTarget(const cv::Rect& human);

private:
  // called by thread in launch
  void run();

  // romaric tests
  void normalCruise(const ardrone::NavData& data);
  void pilotageAuto();
  void stabiliseVY(const ardrone::NavData& data, const int targetSpeed);
  void stabiliseVX(const ardrone::NavData& data, const int targetSpeed);
  peopledetect::HumanInfo getBestLikelyhood(std::vector<peopledetect::HumanInfo>*
					    humanList);
  void followPoints(const std::vector<cv::Point2f>* points);
  // followKalman is the new followPoints(...)
  void followKalman();
  bool isInside(const peopledetect::HumanInfo& human,
		const cv::Point2f& point);
  float computeLikelyhoodHuman(const peopledetect::HumanInfo& human,
			       const std::vector<cv::Point2f>* points);
  void targetAnglePoint(const float x);
  void targetAnglePoint(const cv::Rect& rectangle);

  // lock
  void setTarget(const cv::Rect& human);

  // math with angle
  int angleFloatToInt(float angle);
  int angleAdd(int angle1, int angle2);
  int angleSub(int angle1, int angle2);
  

  // to follow target
  void followPeople();
  void followMovements(ardrone::NavData& data);
  void stabiliseAngle(const ardrone::NavData& data);
  void setDistanceToTarget(const cv::Rect& human);
  void setDistanceToTarget(const std::vector<cv::Point2f>* points);
  void setDistanceToTarget(const int sizeY);


  std::thread controller_;
  std::atomic<bool> manualFlight_;
  std::atomic<bool> exit_;
  collision::Collision& collision_;
  peopledetect::PeopleDetectHelper& people_;

  Kalman kalman_;
  int kalmanCounter_;
  
  // for target
  int targetAngle_; //  >= 0 and < 360
  double targetDistance_;
  // for window
  cv::Rect* humanTarget;
  std::mutex mutexTarget_;

};
}

#endif /* STABILISATION */
