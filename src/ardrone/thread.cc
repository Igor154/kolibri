#include "thread.hh"

#ifdef __cplusplus
extern "C"
{
#endif

#include "exec.hh"

#ifdef __cplusplus
}
#endif

namespace ardrone
{
  Thread::Thread(int argc, char* argv[])
    : _argc(argc)
    , _argv(argv)
    , _thread(&Thread::run, this)
  {
  }

  Thread::~Thread()
  {
    ardrone_signal_exit();
    _thread.join();
  }

  bool Thread::isConnected() const
  {
    return ardrone_is_connected();
  }

  void Thread::run()
  {
    ardrone_signal_start();
    ardrone_main(_argc, _argv);
  }
}
