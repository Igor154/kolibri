#include <iostream>


#include "collision.hh"
#include "../tools/scoped-timer.hh"
#include "../Debug.hh"

namespace collision {


  Collision::Collision(unsigned int targetFps):
    targetFps_(targetFps),
    lastExecutionTime_(std::chrono::milliseconds(0)) {
    init();
  }

  Collision::~Collision() {
  }

  void Collision::update(cv::Mat mat) {
    tools::ScopedTimer<std::chrono::milliseconds> timer(std::chrono::milliseconds(1000 / targetFps_));

    if (!mat.empty()) {
      analyseImage(mat);
    }

    lastExecutionTime_ = timer.getElapsedTime();
  }

  void Collision::init() {
    firstCall_ = true;
    dangerCounter_ = 0;
    lost_ = false;
    dangerLeft_ = 0.0f;
    dangerRight_ = 0.0f;
  }

  std::chrono::milliseconds Collision::getLastExecutionTime() const
  {
    return lastExecutionTime_;
  }

  void Collision::copyDebugFrameTo(cv::Mat& mat)
  {
    std::lock_guard<std::mutex> lock(mutexDebugFrame_);
    debugFrame_.copyTo(mat);
  }

  void Collision::setDebugFrame(const cv::Mat& mat)
  {
    std::lock_guard<std::mutex> lock(mutexDebugFrame_);
    mat.copyTo(debugFrame_);
  }

  bool Collision::isLeft(cv::Point2f& point, int width_middle) {
    return point.x < width_middle;
  }


  std::chrono::system_clock::time_point Collision::getTimeNow() {
    return std::chrono::high_resolution_clock::now();
  }

  void Collision::sortListWidth(std::vector<cv::Point2f>& listAllCurrent,
				std::vector<cv::Point2f>& listAllPrev) {

    for(unsigned int i = 0; i < listAllCurrent.size(); i++) {
      for(unsigned int j = i; j < listAllCurrent.size() - 1; j++) {
	if(listAllCurrent[j].x > listAllCurrent[j + 1].x) {
	  std::swap(listAllCurrent[j + 1], listAllCurrent[j]);
	  std::swap(listAllPrev[j + 1], listAllPrev[j]);
	}
	assert(listAllCurrent[j].x <= listAllCurrent[j + 1].x);
      }
    }

  }


  void Collision::sortListHeight(std::vector<cv::Point2f>& listAllCurrent,
				 std::vector<cv::Point2f>& listAllPrev) {

    for(unsigned int i = 0; i < listAllCurrent.size(); i++) {
      for(unsigned int j = i; j < listAllCurrent.size() - 1; j++) {
	if(listAllCurrent[j].y > listAllCurrent[j + 1].y) {
	  std::swap(listAllCurrent[j + 1], listAllCurrent[j]);
	  std::swap(listAllPrev[j + 1], listAllPrev[j]);
	}
	assert(listAllCurrent[j].y <= listAllCurrent[j + 1].y);
      }
    }

  }


  float Collision::getBestDangerHeight(std::vector<cv::Point2f>& listAllCurrent,
				       std::vector<cv::Point2f>& listAllPrev,
				       cv::Mat& image) {
    assert(listAllCurrent.size() == listAllPrev.size());
    lost_ = listAllCurrent.size() <= THRESHOLD_LOST;
    if(listAllCurrent.size() == 0) {
      return 0.0f;
    }
    sortListHeight(listAllCurrent, listAllPrev);
    const unsigned int quarter = listAllCurrent.size() >> 2;
    const unsigned int beginIndex = quarter;
    const unsigned int endIndex = listAllCurrent.size() - quarter;
    float currentSum = 0.0f;
    float* values = new float[listAllCurrent.size()];

    for(unsigned int i = 0; i < listAllCurrent.size(); i++) {
      const float diff = listAllCurrent[i].y - listAllPrev[i].y;
      values[i] = diff;
      currentSum = currentSum + diff;
    }
    const float sum = currentSum;
    currentSum = 0.0f;
    for(unsigned int i = 0; i < quarter; i++) {
      currentSum = currentSum + values[i];
    }
    const float sumQuarter = currentSum;

    unsigned int nbrLeft = beginIndex;
    unsigned int nbrRight = endIndex;
    assert(nbrLeft + nbrRight == listAllCurrent.size());

    float sumLeft = sumQuarter;
    float sumRight = sum - sumQuarter;
    float maxDanger = FLT_MIN;
    int maxIndex = 0;
    for(unsigned int i = beginIndex; i < endIndex; i++) {
      const float averageLeft = sumLeft / nbrLeft;
      const float averageRight = sumRight / nbrRight;
      const float currentDanger = averageRight - averageLeft;
      if(currentDanger == currentDanger) { // != Nan
	if(currentDanger > maxDanger) {
	  maxDanger = currentDanger;
	  maxIndex = i;
	}
      }
      //std::cout << sumLeft << " " << sumRight << " " << sum << std::endl;
      //assert(sumLeft + sumRight == sum);
      sumLeft = sumLeft + values[i];
      sumRight = sumRight - values[i];
      nbrLeft++;
      nbrRight--;
      assert(nbrLeft + nbrRight == listAllCurrent.size());
    }

    delete[](values);
    if(maxDanger < 0.0f) {
      return 0.0f;
    } else {
      line(image,
	   cv::Point2f(0, listAllCurrent[maxIndex].y),
	   cv::Point2f(Resource::getInstance().getWidth(), listAllCurrent[maxIndex].y),
	   cv::Scalar(0,0,255));
      return maxDanger;
    }
  }


  float Collision::getBestDangerWidth(std::vector<cv::Point2f>& listAllCurrent,
				      std::vector<cv::Point2f>& listAllPrev,
				      cv::Mat& image) {
    assert(listAllCurrent.size() == listAllPrev.size());
    lost_ = listAllCurrent.size() <= THRESHOLD_LOST;
    if(listAllCurrent.size() == 0) {
      return 0.0f;
    }
    sortListWidth(listAllCurrent, listAllPrev);
    const unsigned int quarter = listAllCurrent.size() >> 2;
    const unsigned int beginIndex = quarter;
    const unsigned int endIndex = listAllCurrent.size() - quarter;
    float currentSum = 0.0f;
    float* values = new float[listAllCurrent.size()];

    for(unsigned int i = 0; i < listAllCurrent.size(); i++) {
      const float diff = listAllCurrent[i].x - listAllPrev[i].x;
      values[i] = diff;
      currentSum = currentSum + diff;
    }
    const float sum = currentSum;
    currentSum = 0.0f;
    for(unsigned int i = 0; i < quarter; i++) {
      currentSum = currentSum + values[i];
    }
    const float sumQuarter = currentSum;

    unsigned int nbrLeft = beginIndex;
    unsigned int nbrRight = endIndex;
    assert(nbrLeft + nbrRight == listAllCurrent.size());

    float sumLeft = sumQuarter;
    float sumRight = sum - sumQuarter;
    float maxDanger = FLT_MIN;
    float maxDangerLeft = 0.0f;
    float maxDangerRight = 0.0f;
    int maxIndex = 0;
    for(unsigned int i = beginIndex; i < endIndex; i++) {
      const float averageLeft = sumLeft / nbrLeft;
      const float averageRight = sumRight / nbrRight;
      const float currentDanger = averageRight - averageLeft;
      if(currentDanger == currentDanger) { // != Nan
	if(currentDanger > maxDanger) {
	  maxDanger = currentDanger;
	  maxIndex = i;
	  maxDangerLeft = averageLeft;
	  maxDangerRight = averageRight;
	}
      }
      //std::cout << sumLeft << " " << sumRight << " " << sum << std::endl;
      //assert(sumLeft + sumRight == sum);
      sumLeft = sumLeft + values[i];
      sumRight = sumRight - values[i];
      nbrLeft++;
      nbrRight--;
      assert(nbrLeft + nbrRight == listAllCurrent.size());
    }
    dangerLeft_ = -maxDangerLeft;
    dangerRight_ = maxDangerRight;

    delete[](values);
    if(maxDanger < 0.0f) {
      return 0.0f;
    } else {
      line(image,
	   cv::Point2f(listAllCurrent[maxIndex].x, 0),
	   cv::Point2f(listAllCurrent[maxIndex].x,
		       Resource::getInstance().getHeight()),
	   cv::Scalar(0,0,255));
      return maxDanger;
    }
  }

  float distance(cv::Point2f point1, cv::Point2f point2) {
    const float dx = point1.x - point2.x;
    const float dy = point1.y - point2.y;
    return (dx * dx) + (dy * dy);
  }

  bool Collision::removeUselessPoints(std::vector<cv::Point2f>& listAllCurrent,
				      std::vector<cv::Point2f>& listAllPrev) {
    if(Debug::debug) {
      assert(listAllCurrent.size() == listAllPrev.size());
    }
    const unsigned int size = listAllCurrent.size();
    for(unsigned int i = 0; i < size; i++) {
      for(unsigned int j = i + 1; j < size; j++) {
	if(Debug::debug) {
	  assert(i < size);
	  assert(j < size);
	  assert(i != j);
	  assert(j > i);
	}
	if(distance(listAllCurrent[i], listAllCurrent[j])
	   < MINIMAL_DISTANCE_BETWEEN_POINTS * MINIMAL_DISTANCE_BETWEEN_POINTS) {
	  assert(listAllCurrent.size() == listAllPrev.size());
	  listAllCurrent.erase(listAllCurrent.begin() + i);
	  listAllPrev.erase(listAllPrev.begin() + i);
	  assert(listAllCurrent.size() == listAllPrev.size());
	  assert(listAllCurrent.size() == size - 1);
	  assert(listAllPrev.size() == size - 1);
	  return true; // remove just 1 point per call
	}
      }
    }
    if(Debug::debug) {
      assert(listAllCurrent.size() == listAllPrev.size());
    }
    return false;

  }

  cv::Scalar Collision::getRandomColor() {
    return cv::Scalar(rand() % 255, rand() % 255, rand() % 255);
  }

  void Collision::analyseImage(cv::Mat& currentFrame) {
    cv::Mat gray;
    cv::Mat image; // for printing
    cv::Mat frame;
    if(firstCall_) {
      firstCall_ = false;
      prevTime_ = getTimeNow();
    }
    std::chrono::system_clock::time_point now = getTimeNow();
    const long time = std::chrono::duration_cast<std::chrono::nanoseconds>(now - prevTime_).count();

    currentFrame.copyTo(frame);
    frame.copyTo(image);

    const cv::TermCriteria termcrit(cv::TermCriteria::COUNT|
				    cv::TermCriteria::EPS,20,0.03);
    const cv::Size subPixWinSize(10,10);
    //const cv::Size winSize(31,31);
    const cv::Size winSize(5,5);

    //cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

    cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
    //cv::cvtColor(currentFrame, gray, cv::COLOR_BGR2GRAY);
    //cv::cvtColor(prevFrame, prevGray, cv::COLOR_BGR2GRAY);

    std::thread peopleFollowThread(&Collision::updatePointsToTrack,
    				   this, prevGray_, gray);

    if(points_[0].size() < (MAX_COUNT_FEATURES * 6) / 10) {
      std::vector<cv::Point2f> pointsToAdd;
      cv::goodFeaturesToTrack(gray,
			      pointsToAdd,
			      MAX_COUNT_FEATURES - points_[0].size(),
			      0.01,
			      10,
			      cv::Mat(),
			      3,
			      0,
			      0.04);
      cv::cornerSubPix(gray, pointsToAdd, subPixWinSize,
		       cv::Size(-1,-1), termcrit);
      for(unsigned int i = 0; i < pointsToAdd.size(); i++) {
	points_[0].push_back(pointsToAdd[i]);
      }
      // add points to track
      /*{
	const std::lock_guard<std::mutex> lock(mutexToTrack_);
	for(unsigned int i = 0; i < pointsToTrack_.size(); i++) {
	  points_[1].push_back(pointsToTrack_[i]);
	}
	}*/
      //std::cout << "added features : " << pointsToAdd.size() << std::endl;
      //addRemovePt_ = false;
    } else if(!points_[0].empty()) {
      std::vector<uchar> status;
      std::vector<float> err;
      if(prevGray_.empty())
	gray.copyTo(prevGray_);
      cv::calcOpticalFlowPyrLK(prevGray_,
			       gray,
			       points_[0],
			       points_[1],
			       status,
			       err,
			       winSize,
			       3 /* 3 */,
			       termcrit,
			       0,
			       0.001);

      size_t i, k;
      for(i = k = 0; i < points_[1].size(); i++) {
	if(!status[i])
	  continue;
	
	points_[1][k++] = points_[1][i];
	
	///*
	circle(image, points_[1][i], 3, cv::Scalar(0,255,0), -1, 8);
	circle(image, points_[0][i], 3, cv::Scalar(255,0,0), -1, 8);
	line(image, points_[1][i], points_[0][i], cv::Scalar(0,0,255));
	//*/
      }
      points_[1].resize(k);
      points_[0].resize(k);

      const double pointSize = (double)points_[1].size();
      
      while(points_[1].size() > MAX_COUNT_FEATURES
	    && removeUselessPoints(points_[1], points_[0])) {
      }

      if(Debug::debug) {
	assert(points_[1].size() == points_[0].size());
	assert(points_[1].size() <= MAX_COUNT_FEATURES);
	assert(points_[0].size() <= MAX_COUNT_FEATURES);
      }

      //float totalDanger = averageRight - averageLeft;
      const float bestDangerWidth = getBestDangerWidth(points_[1],
						       points_[0], image);
      if(Debug::debug) {
	assert(points_[1].size() == points_[0].size());
	assert(points_[1].size() <= MAX_COUNT_FEATURES);
	assert(points_[0].size() <= MAX_COUNT_FEATURES);
      }
      const float bestDangerHeight = getBestDangerHeight(points_[1],
							 points_[0], image);
      if(Debug::debug) {
	assert(points_[1].size() == points_[0].size());
	assert(points_[1].size() <= MAX_COUNT_FEATURES);
	assert(points_[0].size() <= MAX_COUNT_FEATURES);
      }
      const double bestDanger = bestDangerWidth + bestDangerHeight;

      const double timeFloat = (double)time;
      double totalDanger = (bestDanger * timeFloat) / pointSize;

      if(totalDanger == totalDanger) { // return false if NaN
	dangerCounter_ = dangerCounter_ + totalDanger;
	dangerCounter_ = dangerCounter_ * getTimeCoef(timeFloat);

	// print
	if(dangerCounter_ > 0) {
	  const double valutToPrint = (dangerCounter_ / THRESHOLD_DANGER) * 100;
	  cv::circle(image, cv::Point2f(160, 200), valutToPrint, cv::Scalar(0,0,255), -1, 8);
	}
	if(dangerLeft_ > 0)
	  cv::circle(image, cv::Point2f(80, 200), dangerLeft_, cv::Scalar(0,0,255), -1, 8);
	if(dangerRight_ > 0)
	  cv::circle(image, cv::Point2f(240, 200), dangerRight_, cv::Scalar(0,0,255), -1, 8);
      }

      //std::cout << "dangerCounter_ : " << dangerCounter_ << std::endl;
      //std::cout << "dangerLeft_ : " << dangerLeft_ << std::endl;
      //std::cout << "dangerRight_ : " << dangerRight_ << std::endl;

      const double MAX_DANGER_COUNTER = THRESHOLD_DANGER * 3.0;
      if(mustStop()) {
	cv::circle(image, cv::Point2f(Resource::getInstance().getWidth() >> 1,
				      Resource::getInstance().getHeight() >> 1),
		   50, cv::Scalar(255, 0, 0), -1, 8);
	if(dangerCounter_ > MAX_DANGER_COUNTER) {
	  dangerCounter_ = MAX_DANGER_COUNTER;
	}
      }

    }


#ifdef TEST_WITH_VIDEO
    //cv::imshow("Debug", image);
    //cv::waitKey(1);
#endif

    setDebugFrame(image);

    std::swap(points_[1], points_[0]);
    peopleFollowThread.join();
    cv::swap(prevGray_, gray);
    prevTime_ = now;

  }

  double Collision::getTimeCoef(double timeFloat) {
    timeFloat = timeFloat / 1000000.0d;
    const double coef = 0.99d;
    double ret = coef;
    while(timeFloat > 0.0d) {
      timeFloat = timeFloat - 1.0d;
      ret = ret * coef;
    }
    return ret;
  }

  void Collision::setNewTarget(const peopledetect::HumanInfo* human) {
    peopleFollow_.setNewTarget(human->getRectangle());
  }
    
  bool Collision::mustStop() {
    return dangerCounter_ > THRESHOLD_DANGER;
  }
  
  void Collision::resetDanger() {
    dangerCounter_ = 0;
  }

  bool Collision::isLost() {
    return lost_;
  }

  float Collision::getDangerLeft() {
    return dangerLeft_;
  }

  float Collision::getDangerRight() {
    return dangerRight_;
  }

  std::vector<cv::Point2f>* Collision::getCopyPointsToTrack() {
    return peopleFollow_.getCopyPointsToTrack();
  }

  void Collision::updatePointsToTrack(const cv::Mat& prevGrayImage,
				      const cv::Mat& grayImage) {
    peopleFollow_.updatePointsToTrack(prevGrayImage, grayImage);
  }
}
