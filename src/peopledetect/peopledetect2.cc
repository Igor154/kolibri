#include <cmath>

#include "peopledetect2.hh"
#include "helper.hh"

#include "../settings-manager.hh"
#include "../Resource.hh"

namespace peopledetect
{
  PeopleDetect2::PeopleDetect2()
    :nbUpWithoutAnyTarget(0),
     latestHumanList_updates(0)
  {}
  PeopleDetect2::~PeopleDetect2(){}

  std::vector<Human>* PeopleDetect2::detectPeople(cv::Mat& src)
  {
    if (Helper::consolePrint)
      std::cout << "peopledetectBegin" << std::endl;

    // timer
    double t = (double)cv::getTickCount();

    //the vector of detected humans to return
    std::vector<Human>* returnVector = new (std::vector<Human>);

    // make sure to make a deepcopy of the image
    cv::Mat imgCopy;
    src.copyTo(imgCopy);
    cv::Mat imgCopyResized = imgCopy;

    // double sized image for better small chr detection
    if (Helper::doubleSizedImage)
    {
      cv::Size size(imgCopy.cols*2, imgCopy.rows*2);
      cv::resize(imgCopy, imgCopyResized, size, 0, 0, cv::INTER_LINEAR);
    }
    std::vector<cv::Rect> found;

    if (SettingsManager::getInstance().isGPUEnabled())
      this->gpu_compute(imgCopyResized, found);
    else // detection cpu
      this->cpu_compute(imgCopyResized, found);

    // remove out of bounds values
    Helper::crop(found, imgCopyResized.cols, imgCopyResized.rows);

    //remove extreme rectangles
    Helper::sanity_check(found);

    // make Humans with the rectangles
    this->makeHumans(imgCopy, found, *returnVector);

    //remove extreme humans
    Human::sanity_check33(this->currentTarget, returnVector);
    
    // replace target with the closest Human
    // if no target sets the likelihood of everyone to 0
    ++this->nbUpWithoutAnyTarget;//postulat changed by updatecurrenttarget
    this->updateCurrentTarget(*returnVector);
    
    //make a copy to keep as latest list
    if (returnVector->size() > 0)
    {
      this->latestHumanList_updates = 0;
      mutex.lock();
      this->latestHumanList.clear();
      for (unsigned int i = 0; i < returnVector->size(); ++i)
	this->latestHumanList.push_back((*returnVector)[i]);
      mutex.unlock();
    }
    else //after a while remove latestHumanList
    {
      if (++this->latestHumanList_updates > Helper::latestHumanList_timeout)
      {
	this->latestHumanList_updates = 0;
	mutex.lock();
	this->latestHumanList.clear();
	mutex.unlock();
      }
    }

    t = (double)cv::getTickCount() - t;

    if (Helper::drawWindow)
      this->drawDebug(returnVector, imgCopy, t);

    if (Helper::consolePrint)
      std::cout << "peopledetect End" << std::endl;

    return returnVector; 
}

void PeopleDetect2::makeHumans(cv::Mat& srcImg,
			       std::vector<cv::Rect>& rects,
			       std::vector<Human>& humans)
{
  for (unsigned int i = 0; i < rects.size(); ++i)
  {
    cv::Rect rect = rects[i];
    if (Helper::doubleSizedImage)
      rect = cv::Rect(rect.x/2, rect.y/2, rect.width/2, rect.height/2);

    Human human(srcImg, rect);

    humans.push_back(human);
  }
}

HumanInfo* PeopleDetect2::setNewTarget(int X, int Y)
{
  //find closest rectangle to target
  mutex.lock();
  if (this->latestHumanList.size() > 0)
  {
    //could have been initialised with first element but that would mean
    //rectCenter would had been calculated twice which is verbose for nothing
    int index = -1;
    int closestDistance = std::numeric_limits<int>::max();

    for (unsigned int i = 0; i < this->latestHumanList.size(); ++i)
    {
      int rectCenterX =this->latestHumanList[i].getRect().x + this->latestHumanList[i].getRect().width/2; 
      int rectCenterY =this->latestHumanList[i].getRect().y + this->latestHumanList[i].getRect().height/2;
    
      //Manhattan distance, no need for exact distances, only relative needed
      if (abs(rectCenterX - X) + abs(rectCenterY - Y) < closestDistance)
      {
	closestDistance = abs(rectCenterX - X) + abs(rectCenterY - Y);
	index = i;
      }
    }
    assert(index >= 0 && "At least one closest target should have been found");

    Human h = this->latestHumanList[index];
    mutex.unlock();

    this->setNewCurrentTarget(h);

    return new Human(h);
  }

  mutex.unlock();

  return nullptr;
}

void PeopleDetect2::removeTarget()
{
  std::lock_guard<std::mutex> lock(this->mutex);
  delete (this->currentTarget);
  this->currentTarget = nullptr;
}

Human* PeopleDetect2::getTargetCopy()
{
  std::lock_guard<std::mutex> lock(this->mutex);
  if (this->currentTarget == nullptr)
    return nullptr;

  Human* h = new Human(*(this->currentTarget));

  return h;
}

//****************************************************************
//
//                 PRIVATE METHODS
//
//****************************************************************
void PeopleDetect2::updateCurrentTarget(std::vector<Human>& humans)
{ 
    mutex.lock();
    if (this->currentTarget != nullptr)
    { 
      Human target = *(this->currentTarget);
      mutex.unlock();
      for (unsigned int i = 0; i < humans.size(); ++i)
        humans[i].likeLihoodUpdate(target);

      unsigned int index = 0;
      float bestScore = -1.;

      //choose the human closest to the target i-e best likelihood
      for (unsigned int i = 0; i < humans.size(); ++i)
      {
        if (humans[i].getLikelihood() > 0.5)
	{
	  if (bestScore < humans[i].getLikelihood())
	  {
	    index = i;
	    bestScore = humans[i].getLikelihood();
	  }
	}
      }
   
      //set a new currentTarget 
      if (bestScore > 0)
      {
	this->nbUpWithoutAnyTarget = 0;
        this->setNewCurrentTarget(humans[index]);
      }
      else// no matching human to target found
      {
	//after a while erase currTarget
	if(this->nbUpWithoutAnyTarget > Helper::nbUpWithoutTargetTolerance)
	{
	  this->nbUpWithoutAnyTarget = 0;
	  this->removeTarget();
	}
      }
    }
    else // currtarget == NULL
    {
      this->nbUpWithoutAnyTarget = 0;
      mutex.unlock();
      //no target so likelihood have no sense here
      for (unsigned int i = 0; i < humans.size(); ++i)
	humans[i].setLikelihood(0);
    }

    if (Helper::consolePrint)
      std::cout << "nbUpWithoutTargetUpdate = ----------" << this->nbUpWithoutAnyTarget << std::endl;
}

void PeopleDetect2::setNewCurrentTarget(Human human)
{
  mutex.lock();
  if (this->currentTarget != NULL)
    delete(this->currentTarget);
    
  if (Helper::consolePrint)
    std::cout<<"peopledetect2: new target set"<<std::endl;

  this->currentTarget = new Human(human);
  mutex.unlock();
}
void PeopleDetect2::recalibrateTarget(const HumanInfo& h)
{
  std::lock_guard<std::mutex> lock(this->mutex);
  delete(this->currentTarget);
  this->currentTarget = new Human((Human&)h);
}
// CPU
void PeopleDetect2::cpu_compute(cv::Mat& src, std::vector<cv::Rect>& found)
{
  cv::HOGDescriptor hog;
  hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
  hog.detectMultiScale(src,
		       found, 
		       0, //hit threshold
		       cv::Size(Helper::stride,Helper::stride), //Stride/le pas de décalage 
		       cv::Size(32,32), //padding: added pixels on the borders
		       1.05, //scale
		       2 //group threshold
			 );
}

//GPU
void PeopleDetect2::gpu_compute(cv::Mat& src, std::vector<cv::Rect>& found)
{
  cv::Size win_size(48,96); //(64, 128) or (48, 96)
  cv::Size win_stride(8,8);

  std::vector<float> detector = cv::gpu::HOGDescriptor::getPeopleDetector48x96();

  cv::gpu::HOGDescriptor gpu_hog(win_size, 
				 cv::Size(16, 16), 
				 win_stride, 
				 win_stride, 
				 9,
				 cv::gpu::HOGDescriptor::DEFAULT_WIN_SIGMA, 
				 0.2,
				 true, //gamma correction
				 cv::gpu::HOGDescriptor::DEFAULT_NLEVELS);

  gpu_hog.setSVMDetector(detector);

  cv::Mat img_aux, img, img_to_show;
  cv::gpu::GpuMat gpu_img;

  // Change format of the image
  cv::cvtColor(src, img_aux, CV_BGR2BGRA);

  // Resize image
  //resize(img_aux, img, Size(args.width, args.height));
  img = img_aux;
  img_to_show = img;

  const float scale_factor = 1.05;
  const unsigned max_win_height = Resource::getInstance().getHeight();
  gpu_hog.nlevels = floor(log((double) max_win_height / (double) win_size.height)
                          / log(scale_factor)); //Max number of HOG window scales

  // Perform HOG classification
  gpu_img.upload(img);
  gpu_hog.detectMultiScale(gpu_img,
			   found, 
			   1.8,//hit_threshold, 
			   win_stride,//win_stride,
			   cv::Size(0, 0), //padding Should be 0 to keep cpu compatibility
			   scale_factor,//scale,
			   3);//group_threshold

}

void PeopleDetect2::drawDebug(std::vector<Human>* returnVector, cv::Mat imgCopy, double time)
{  
  cv::putText(imgCopy,
	      std::to_string((int)(time*1000./cv::getTickFrequency())) + "ms",
	      cv::Point(5, 230), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);

  for (unsigned int i = 0; i < returnVector->size(); ++i)
  {
    const Human& h = (*returnVector)[i];
    //draw name
    cv::putText(imgCopy, /*h.name.c_str()*/ "", 
		cv::Point(
			  h.getRect().x + h.getRect().width / 2, 
			  h.getRect().y + h.getRect().height / 2),
		cv::FONT_HERSHEY_SIMPLEX, 0.5, 
		cv::Scalar(122, 255, 255), 2);
    //draw rectangle
    cv::rectangle(imgCopy, h.getRect(), cv::Scalar(122,255,0));
  }

  //if the target draw another color
  if (this->currentTarget != NULL)
    cv::rectangle(imgCopy, this->currentTarget->getRect(), cv::Scalar(0,255,255));
    
  cv::imshow("PeopleDetect debug", imgCopy);
  cv::waitKey(30);
}

}//namespace
