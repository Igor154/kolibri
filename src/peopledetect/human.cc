#include "human.hh"

namespace peopledetect
{
  const unsigned int Human::NB_HUE_BUCKETS = 10;
  const unsigned int Human::NB_HUE_PER_BUCKET = 180 / NB_HUE_BUCKETS;
  int Human::nb_human_created = 0;

  Human::Human(cv::Mat& src, cv::Rect& selection)
  {
    this->setRect(cv::Rect(selection));

    // init hue buckets
    for (unsigned int i = 0; i<this->NB_HUE_BUCKETS; ++i)
      this->hue_buckets_percent.push_back(0);

    
    cv::Mat subMat = src(selection);
    cv::Mat copy, copy_hsv;
    subMat.copyTo(copy);//deep copy
    
    cv::cvtColor(copy, copy_hsv, CV_RGB2HSV);
    
    // add in hue values
    for(int y = 0; y < copy_hsv.rows; y++)
    {
      for(int x = 0; x < copy_hsv.cols; x++)
      {
	cv::Vec3b hsv= copy_hsv.at<cv::Vec3b>(x,y);
	//int H=hsv.val[0]; //hue
	//int S=hsv.val[1]; //saturation
	//int V=hsv.val[2]; //value
	//printf("H %i  S %i   V %i\n", H, S, V);
	int bucketIndex = hsv.val[0] / this->NB_HUE_PER_BUCKET;

	// assert un peu overkill: pour que le dépassement arrive il faudrait une image de 63245x63245pixels et que tous les pixels se retrouvent dans le même bucket
	assert((unsigned int)(this->hue_buckets_percent[bucketIndex] + 1) > this->hue_buckets_percent[bucketIndex]
	       && "Depassement de valeur unsigned int dans le calcul des hue buckets");
	
	assert((unsigned int)(this->totalPixels + 1) > this->totalPixels
	       && "Depassement de valeur unsigned int dans le calcul de la moyenne des hue buckets");
	

        ++this->hue_buckets_percent[bucketIndex];
        ++this->totalPixels;
      }
    }

    // now compute the proportional percentage for each bucket
    this->compute_mean_hue_buckets();
  }
  Human::~Human()
  {}

  // METHODS
  void Human::compute_mean_hue_buckets()
  {
    float sanityCheck = 0;
    for (unsigned int i = 0; i < this->hue_buckets_percent.size(); ++i)
    {
      float tmp = ((float)(this->hue_buckets_percent[i]) / (float)this->totalPixels) * 100.;
      this->hue_buckets_percent[i] = static_cast<unsigned int>(tmp);
      sanityCheck += tmp;
    }

    float epsilon = 0.001;
    if (abs(sanityCheck - 100.) > epsilon)
    {
      //std::cout << sanityCheck << std::endl;
      assert (sanityCheck == 100. && "Should be 100%");
    }
  }
  void Human::likeLihoodUpdate(Human& target)
  {
    int errorSum = 0;
    for (unsigned int i = 0; i < this->hue_buckets_percent.size(); ++i)
    {
      errorSum += abs((int)this->hue_buckets_percent[i] - (int)target.getHueBuckets()[i]);
    }

    this->setLikelihood((float)(200 - errorSum)/200.);

    if (Helper::consolePrint)
      std::cout << "likelihood set ()()()()()()()()" << this->likelihood << std::endl;
  }

  // GETTERS
  std::vector<unsigned int>& Human::getHueBuckets()
  {
    return this->hue_buckets_percent;
  }
  const cv::Rect& Human::getRect() const
  {
    return getRectangle();
  }
  const cv::Rect& Human::getRectangle() const
  {
    return this->rect;
  }
  float Human::getLikelihood() const
  {
    assert(this->likelihood >= 0.0f);
    assert(this->likelihood <= 1.0f);
    return this->likelihood;
  }

  // SETTERS
  void Human::setRect(cv::Rect rect)
  {
    this->rect = rect;
  }
  void Human::setLikelihood(float f)
  {
    assert(this->likelihood >= 0.0f);
    assert(this->likelihood <= 1.0f);
    this->likelihood = f;
  }

}
