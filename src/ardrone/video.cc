#include "video.hh"

namespace ardrone
{
  static void rotate(const cv::Mat& src, cv::Mat& dst, double angle)
  {
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
    cv::warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
  }

  static void translate(const cv::Mat& src, cv::Mat& dst, int offsetx, int offsety)
  {
    cv::Mat trans_mat = (cv::Mat_<double>(2,3) << 1, 0, offsetx, 0, 1, offsety);
    cv::warpAffine(src, dst, trans_mat, src.size());
  }

  static void lateralStabilisation(const cv::Mat& src, cv::Mat& dst, double phi)
  {
    rotate(src, dst, - phi / 1000.0);
  }

  static void verticalStabilisation(const cv::Mat& src, cv::Mat& dst, double theta)
  {
    translate(src, dst, 0, theta * -0.0043);
  }

  static void fullStabilisation(const cv::Mat& src, cv::Mat& dst, double phi, double theta)
  {
    cv::Mat tmp;
    lateralStabilisation(src, tmp, phi);
    verticalStabilisation(tmp, dst, theta);
  }

  Video::Video()
    : _record(false)
  {
  }

  Video& Video::getInstance()
  {
    static Video instance;
    return instance;
  }

  size_t Video::getWidth() const
  {
    std::lock_guard<std::mutex> lock(_mutex);
    return _current.cols;
  }

  size_t Video::getHeight() const
  {
    std::lock_guard<std::mutex> lock(_mutex);
    return _current.rows;
  }

  void Video::updateFrame(void* data, size_t width, size_t height, double phi, double theta)
  {
    cv::Mat frame(cv::Size(width, height), CV_8UC3, data, cv::Mat::AUTO_STEP);

    {
      std::lock_guard<std::mutex> lock(_mutex);
      _current.release();
      cv::cvtColor(frame, _current, CV_BGR2RGB);
      _currentTheta = theta;
      _currentPhi = phi;
    }
  }

  cv::Mat Video::cloneLastFrame(enum eagleEye stabilisation) const
  {
    cv::Mat frame;
    double currentPhi;
    double currentTheta;

    {
      std::lock_guard<std::mutex> lock(_mutex);
      frame = _current.clone();
      currentPhi = _currentPhi;
      currentTheta = _currentTheta;
    }

    if (stabilisation == NONE || frame.empty())
      return frame;

    cv::Mat frame_stab;

    if (stabilisation == LATERAL)
      lateralStabilisation(frame, frame_stab, currentPhi);
    else if (stabilisation == FULL)
      fullStabilisation(frame, frame_stab, currentPhi, currentTheta);

    return frame_stab;
  }

  void Video::copyLastFrameTo(cv::Mat& mat, enum eagleEye stabilisation) const
  {
    double currentPhi;
    double currentTheta;

    if (stabilisation == NONE)
    {
      std::lock_guard<std::mutex> lock(_mutex);
      _current.copyTo(mat);
      currentPhi = _currentPhi;
      currentTheta = _currentTheta;
    }
    else
    {
      cv::Mat tmp;

      {
        std::lock_guard<std::mutex> lock(_mutex);
        _current.copyTo(tmp);
        currentPhi = _currentPhi;
        currentTheta = _currentTheta;
      }

      if (tmp.empty())
        tmp.copyTo(mat);
      else if (stabilisation == LATERAL)
        lateralStabilisation(tmp, mat, currentPhi);
      else if (stabilisation == FULL)
        fullStabilisation(tmp, mat, currentPhi, currentTheta);
    }
  }

  void Video::startRecording()
  {
    _record = true;
  }

  void Video::stopRecording()
  {
    _record = false;
  }

  bool Video::isRecording() const
  {
    return _record;
  }
}
