#ifndef TOOLS_SCOPED_TIMER_HXX
# define TOOLS_SCOPED_TIMER_HXX

# include <thread>

namespace tools
{
  template <typename T>
  ScopedTimer<T>::ScopedTimer(T minimalDuration)
    : _minimalDuration(minimalDuration)
  {
  }

  template <typename T>
  ScopedTimer<T>::~ScopedTimer()
  {
    T actualDuration(_timer.getElapsedTime());

    if (actualDuration < _minimalDuration)
      std::this_thread::sleep_for(_minimalDuration - actualDuration);
  }

  template <typename T>
  T ScopedTimer<T>::getElapsedTime() const
  {
    return _timer.getElapsedTime();
  }
}

#endif /* !TOOLS_SCOPED_TIMER_HXX */
